# MoEnchantments
### For fabric. If you want forge then port it; This project it Licensed under GPL.
### [Change log](https://gitlab.com/biom4st3r/moenchantments/-/blob/default/CHANGELOG)
### [Enchantment descriptions(Warning spoilers)](https://gitlab.com/biom4st3r/moenchantments/-/blob/default/EnchantmentDescriptions.md)
### [More Up-to-date description](https://gitlab.com/biom4st3r/moenchantments/-/blob/default/README.md)
### Please submit and issues you find to my [Gitlab](https://gitlab.com/biom4st3r/moenchantments/issues) or Ping me on one of the Fabric Discords!
This mod adds multiple enchantments with more to come!
* Veining - Pickaxes
* Timber - Axes
* Alpha Fire - Shovels, Axes, and Pickaxes
* Familiarity - Axe and Swords
* Curse of the End - Armor
* Imbued - Axes and Swords
* Soulbound - All items
* Chaos - Bows
* Curse of the Trooper - Bows
* Marksman - Bows
* Grapnel - Bows
* Black Hole - Tools
* Slippery - Armor, Tools, Weapons
* Filter Feeder - Helmets
Configs are located at `moenchantconfig.json`
#
### Don't like the default settings of an Enchantment? No problem! In the `configmoenchantment_overrides.json` you can now configure overrides for most of the enchantment settings!

Template:
```json
{
    "treefeller": { // Internal name of the enchantment as seen in the /enchant command
        "enabled":true, // Whether or not the enchantment is registered
        "min_power":"level * 5", // This supports all manner of expressions(Most of which is unnecessary)
        /**
        "3 * sin(level) - 2 / (level - 2)" for example. the ONLY valid variable name is level
        https://www.objecthunter.net/exp4j/
        */
        "max_power":"level * 10",
        "treasure":false, // Whether this is treated similarly to mending 
        "curse":false, // Whether this is a curse
        "max_level":5, // Max level. this usually won't make sense to override
        "min_level":10 // Max level. this usually won't make sense to override
    }
}
```
overrides take priority over all entries in `moenchantconfig.json`

If you like my work and would like to support me consider donating to my Ko-fi!

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/K3K033W6X)

# Special Thanks
## Provided Enchantment Ideas
* Mr Cloud on Gitlab
* cryum on Gitlab
* Korr
* Rm on Gitlab
## Translations
* reirose
* D0L3BUR
* Ҝờţأķ 
* xuyu0v0 