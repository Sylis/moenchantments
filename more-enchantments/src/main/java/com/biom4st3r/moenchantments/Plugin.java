package com.biom4st3r.moenchantments;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;

import net.fabricmc.loader.api.FabricLoader;

import com.biom4st3r.moenchantments.OpcodeMethodVisitor.OpcodeClassVisitor;
import com.google.common.collect.Lists;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;
import it.unimi.dsi.fastutil.objects.Object2BooleanMap;
import it.unimi.dsi.fastutil.objects.Object2BooleanOpenHashMap;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

/**
 * Plugin
 */
public class Plugin implements IMixinConfigPlugin {
    private static final BioLogger logger = new BioLogger("MoEnchantmentMxnPlugin");

    @Override
    public String getRefMapperConfig() {
        return null;
    }

    @SuppressWarnings("all")
    public static Object2BooleanMap<String> disabler = make(new Object2BooleanOpenHashMap<>(), map  ->  {
        String prefix = "com.biom4st3r.moenchantments.mixin.";
        map.put(prefix + "soulbound/PlayerInventoryMxn", MoEnchantsConfig.config.EnableSoulbound);
        map.put(prefix + "soulbound/ServerPECopyFromMxn", MoEnchantsConfig.config.EnableSoulbound);
        map.put(prefix + "bowaccuracy/ProjectileEntityMxn", !FabricLoader.getInstance().isModLoaded("extrabows"));                                                                                                                      
        map.put(prefix + "FillFix", FabricLoader.getInstance().isDevelopmentEnvironment());
        map.put(prefix + "SkipEulaMxn", FabricLoader.getInstance().isDevelopmentEnvironment());
        map.put(prefix + "SpeedDisplay", false);
        map.put(prefix + "SlaughterWatchDogMxn", FabricLoader.getInstance().isDevelopmentEnvironment());
    });

    public static <T> T make(T obj, Consumer<T> consumer) {
        consumer.accept(obj);
        return obj;
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        boolean b = disabler.getOrDefault(mixinClassName, true);
        logger.debug("Applying %s: %s", mixinClassName, b);
        return b;
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {

    }

    @Override
    public List<String> getMixins() {
        return null;
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {
        
    }

    static private boolean hasFlag(int access, int flag) {
        return (access & flag) != 0;
    }

    static Predicate<Integer> isFinal = i  ->  hasFlag(i, Opcodes.ACC_FINAL);
    static Predicate<Integer> isPublic = i  ->  hasFlag(i, Opcodes.ACC_PUBLIC);
    static Predicate<Integer> isPrivate = i  ->  hasFlag(i, Opcodes.ACC_PRIVATE);
    static Predicate<Integer> isProtected = i  ->  hasFlag(i, Opcodes.ACC_PROTECTED);
    static Predicate<Integer> isPackagePrivate = i  ->  !hasFlag(i, Opcodes.ACC_PUBLIC | Opcodes.ACC_PROTECTED | Opcodes.ACC_PRIVATE);
    static Predicate<Integer> isInterface = i  ->  hasFlag(i, Opcodes.ACC_INTERFACE);
    static Predicate<Integer> isEnum = i  ->  hasFlag(i, Opcodes.ACC_ENUM);

    public void executeOrder66() {
        ScanResult result = new ClassGraph().enableAllInfo().scan();
        List<String> clazzes = Lists.newArrayList();
        result.getAllClasses().filter(clazz  ->  clazz.getName().startsWith("net.minecraft")).forEach(clazzInfo  ->  {
            clazzes.add(clazzInfo.getName().replace(".", "/"));
        });
        List<String> widener = Lists.newArrayList("accessWidener\tv1\tnamed");
        clazzes
            // Stream of Class name strings
            .stream()//.filter(c -> c.startsWith("net/minecraft/loot"))
            .map(clazz  ->  {
                ClassReader reader = null;
                try {
                    reader = new ClassReader(clazz);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ClassNode cnode = new ClassNode(Opcodes.ASM8);
                reader.accept(cnode, ClassReader.SKIP_CODE|ClassReader.SKIP_DEBUG|ClassReader.SKIP_FRAMES);
                return cnode;
            })
            .filter(cn -> isInterface.negate().test(cn.access) )
            .forEach(cn ->
            {
                if (isPublic.negate().test(cn.access)) {
                    if (isEnum.test(cn.access)) widener.add("# Enum");
                    // if (isInterface.test(cn.access)) widener.add("# Interface");
                    if (isPackagePrivate.test(cn.access)) widener.add("# Package Private");
                    widener.add(String.format("%s\t%s\t%s", "accessible","class",cn.name));
                }
                if (isFinal.test(cn.access)) {
                    widener.add(String.format("%s\t%s\t%s", "extendable","class",cn.name));
                }

                if (isEnum.test(cn.access)) return;
                
                cn.methods
                    .stream()//.filter(mn -> false)
                    .forEach(mn ->
                    {
                        if (isPublic.negate().test(mn.access)) {
                            widener.add(String.format("%s\t%s\t%s\t%s\t%s", "accessible","method",cn.name, mn.name, mn.desc));
                        }
                        if (isFinal.test(mn.access)) {
                            widener.add(String.format("%s\t%s\t%s\t%s\t%s", "extendable","method",cn.name, mn.name, mn.desc));
                        }
                    });
                cn.fields
                    .stream()//.filter(mn -> false)
                    .forEach(fn ->
                    {
                        if (isPublic.negate().test(fn.access)) {
                            widener.add(String.format("%s\t%s\t%s\t%s\t%s", "accessible","field",cn.name, fn.name, fn.desc));
                        }
                        if (isFinal.test(fn.access)) {
                            widener.add(String.format("%s\t%s\t%s\t%s\t%s", "mutable","field",cn.name, fn.name, fn.desc));
                        }
                    });
                if (isPublic.negate().or(isFinal).test(cn.access)) widener.add("\n");
            });
        File file = new File(FabricLoader.getInstance().getConfigDir().toString(),"widenme.baby");
        try {
            System.out.println("writing widener");
            FileWriter writer = new FileWriter(file);
            writer.write(String.join("\n", widener));//.stream()/*.filter(s -> !s.startsWith("#"))*/.collect(Collectors.toList())));
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLoad(String mixinPackage) {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        OpcodeClassVisitor cv = OpcodeMethodVisitor.newCv(cw);
        cv.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC, "FFFF", null, "Ljava/lang/Object;", null);
        OpcodeMethodVisitor mv = cv.visitMethod(Opcodes.ACC_PUBLIC|Opcodes.ACC_STATIC|Opcodes.ACC_FINAL, "FFFF", "()V", null, null);
        {
            mv.visitCode();

            mv.RETURN();
            mv.visitMaxs(1, 1);

            mv.visitEnd();
        }
    }
}