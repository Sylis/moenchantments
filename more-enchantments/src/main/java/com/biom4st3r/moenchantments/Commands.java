package com.biom4st3r.moenchantments;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import net.minecraft.command.CommandSource;
import net.minecraft.command.argument.IdentifierArgumentType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContextTypes;
import net.minecraft.loot.entry.LootPoolEntry;
import net.minecraft.loot.function.EnchantRandomlyLootFunction;
import net.minecraft.loot.function.EnchantWithLevelsLootFunction;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.tag.Tag;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.fabricmc.fabric.api.tag.TagRegistry;

import com.google.common.collect.Lists;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;

public class Commands implements CommandRegistrationCallback  {

    @Override
    public void register(CommandDispatcher<ServerCommandSource> dispatcher, boolean dedicated) {
        dispatcher.register(moenchantments);
        dispatcher.register(moenchantmentsItems);
    }

    static enum SuggestTag {
        ARMORS,
        AXES,
        BOOTS,
        BOWS,
        CHESTPLATES,
        EVERYTHING,
        HELMS,
        LEGGINGS,
        PICKAXES,
        SHOVELS,
        SWORDS,
        TOOLS,
        WEAPONS,
        ;
        public final Tag<Item> tag;
        SuggestTag() {
            this.tag = TagRegistry.item(new Identifier(ModInit.MODID, this.name().toLowerCase()));
        }
        public static Stream<String> strings() {
            return Stream.of(SuggestTag.values()).map(t ->t.name());
        }
    }
    static enum SuggestEnchantmentType {
        ENCHANT_RANDOMLY,
        ENCHANT_WITH_LEVELS
    }
    static LiteralArgumentBuilder<ServerCommandSource> moenchantmentsItems = CommandManager
        .literal("moenchant_items")
        .requires(s ->s.getEntity() instanceof ServerPlayerEntity && s.getEntity().hasPermissionLevel(2))
        .then(CommandManager.argument("item", IdentifierArgumentType.identifier())
            .suggests((context, builder) -> {
                return CommandSource.suggestIdentifiers(Registry.ITEM.getIds(), builder);
            })
            .then(CommandManager.literal(SuggestEnchantmentType.ENCHANT_RANDOMLY.name())
                .executes(serverCommandSourceCommandContext -> {
                    ServerPlayerEntity player = serverCommandSourceCommandContext.getSource().getPlayer();
                    Identifier tag = serverCommandSourceCommandContext.getArgument("item", Identifier.class);
                    List<ItemStack> list = Lists.newArrayList();
                    while(list.size() < 54) {
                        list.addAll(enchantedRandomly(LootHelper.itemEntryOf(Registry.ITEM.get(tag)), player));
                    }
                    Collections.shuffle(list);
                    try {
                        player.openHandledScreen(LootHelper.getScreenWithItems(list));
                    } catch(Throwable t) {
                        t.printStackTrace();
                    }
                    return 0;
                })
            )
            .then(CommandManager.literal(SuggestEnchantmentType.ENCHANT_WITH_LEVELS.name())
                .then(CommandManager.argument("level", IntegerArgumentType.integer(0, 255))
                    .then(CommandManager.argument("withTreasure", BoolArgumentType.bool())
                        .executes(serverCommandSourceCommandContext -> {
                            ServerPlayerEntity player = serverCommandSourceCommandContext.getSource().getPlayer();
                            int levels = serverCommandSourceCommandContext.getArgument("level", Integer.class);
                            Identifier tag = serverCommandSourceCommandContext.getArgument("item", Identifier.class);
                            boolean treasure = serverCommandSourceCommandContext.getArgument("withTreasure", Boolean.class);
                            List<ItemStack> list = Lists.newArrayList();
                            while(list.size() < 54) {
                                list.addAll(enchantWithLevels(LootHelper.itemEntryOf(Registry.ITEM.get(tag)), player, levels, treasure));
                            }
                            Collections.shuffle(list);
                            try {
                                player.openHandledScreen(LootHelper.getScreenWithItems(list));
                            } catch(Throwable t) {
                                t.printStackTrace();
                            }
                            return 0;
                        })
                    )
                )
            )
        )
        .executes(serverCommandSourceCommandContext -> {
            serverCommandSourceCommandContext.getSource().sendFeedback(new LiteralText("Nope"), false);
            return 0;
        });
    static LiteralArgumentBuilder<ServerCommandSource> moenchantments = CommandManager
        .literal("moenchantments")
        .requires(s ->s.getEntity() instanceof ServerPlayerEntity && s.getEntity().hasPermissionLevel(2))
        .then(CommandManager.argument("tag", StringArgumentType.word())
            .suggests((context, builder) -> {
                return CommandSource.suggestMatching(SuggestTag.strings(), builder);
            })
            .then(CommandManager.literal(SuggestEnchantmentType.ENCHANT_RANDOMLY.name())
                .executes(serverCommandSourceCommandContext -> {
                    ServerPlayerEntity player = serverCommandSourceCommandContext.getSource().getPlayer();
                    String tag = serverCommandSourceCommandContext.getArgument("tag", String.class);
                    List<ItemStack> list = Lists.newArrayList();
                    while(list.size() < 54) {
                        list.addAll(enchantedRandomly(LootHelper.tagEntryOf(SuggestTag.valueOf(tag).tag, false), player));
                    }
                    Collections.shuffle(list);
                    try {
                        player.openHandledScreen(LootHelper.getScreenWithItems(list));
                    } catch(Throwable t) {
                        t.printStackTrace();
                    }
                    return 0;
                })
            )
            .then(CommandManager.literal(SuggestEnchantmentType.ENCHANT_WITH_LEVELS.name())
                .then(CommandManager.argument("level", IntegerArgumentType.integer(0, 255))
                    .then(CommandManager.argument("withTreasure", BoolArgumentType.bool())
                        .executes(serverCommandSourceCommandContext -> {
                            ServerPlayerEntity player = serverCommandSourceCommandContext.getSource().getPlayer();
                            int levels = serverCommandSourceCommandContext.getArgument("level", Integer.class);
                            String tag = serverCommandSourceCommandContext.getArgument("tag", String.class);
                            boolean treasure = serverCommandSourceCommandContext.getArgument("withTreasure", Boolean.class);
                            List<ItemStack> list = Lists.newArrayList();
                            while(list.size() < 54) {
                                try {
                                    list.addAll(enchantWithLevels(LootHelper.tagEntryOf(SuggestTag.valueOf(tag).tag, false), player, levels, treasure));
                                }catch(Throwable t) {
                                    t.printStackTrace();
                                }
                            }
                            Collections.shuffle(list);
                            try {
                                player.openHandledScreen(LootHelper.getScreenWithItems(list));
                            } catch(Throwable t) {
                                t.printStackTrace();
                            }
                            return 0;
                        })
                    )
                )
            )
        )
        .executes(serverCommandSourceCommandContext -> {
            serverCommandSourceCommandContext.getSource().sendFeedback(new LiteralText("Nope"), false);
            return 0;
        });

    private static List<ItemStack> enchantedRandomly(LootPoolEntry.Builder<?> builder, ServerPlayerEntity player) {
        return LootHelper.tableBuilder()
        .type(LootContextTypes.CHEST)
        .pool(LootHelper
            .poolBuilder()
            .rolls(LootHelper.constantRange(1))
            .with(builder)
            .apply(EnchantRandomlyLootFunction.builder())
        ).build().generateLoot(LootHelper.newContext(player));
    }
    private static List<ItemStack> enchantWithLevels(LootPoolEntry.Builder<?> builder, ServerPlayerEntity player, int levels, boolean withTreasure) {
        EnchantWithLevelsLootFunction.Builder function = EnchantWithLevelsLootFunction.builder(LootHelper.constantRange(levels));
        if (withTreasure) function.allowTreasureEnchantments();
        return LootHelper.tableBuilder()
        .type(LootContextTypes.CHEST)
        .pool(LootHelper
            .poolBuilder()
            .rolls(LootHelper.constantRange(1))
            .with(builder)
            .apply(function))
        .build()
        .generateLoot(LootHelper.newContext(player));
    }
}
