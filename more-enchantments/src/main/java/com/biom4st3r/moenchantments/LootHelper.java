package com.biom4st3r.moenchantments;

import java.util.List;
import java.util.Random;
import java.util.function.Function;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.BinomialLootTableRange;
import net.minecraft.loot.ConstantLootTableRange;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.LootTableRange;
import net.minecraft.loot.UniformLootTableRange;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.entry.LeafEntry;
import net.minecraft.loot.entry.LootPoolEntry;
import net.minecraft.loot.entry.TagEntry;
import net.minecraft.screen.GenericContainerScreenHandler;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.tag.Tag;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.math.Vec3d;

import com.biom4st3r.moenchantments.reflection.CtorRef;
import com.google.common.collect.ImmutableMap;

public interface LootHelper {
    CtorRef<TagEntry> tagEntryCtor = CtorRef.getCtor(TagEntry.class, 0);
    CtorRef<LootContext> lootContextCtor = CtorRef.getCtor(LootContext.class, 0);

    static LootTableRange constantRange(int i) {
        return ConstantLootTableRange.create(i);
    }
    static LootTableRange uniformRange(int min, int max) {
        return UniformLootTableRange.between(min, max);
    }
    static LootTableRange binomialRange(int n, int p) {
        return BinomialLootTableRange.create(n, p);
    }

    static LootPoolEntry.Builder<?> tagEntryOf(Tag<Item> items, boolean expandTag) {
        return LeafEntry.builder((weight, quality, conditions, functions) -> {
            return tagEntryCtor.newInstance(items, expandTag, weight, quality, conditions, functions, null);
        });
    }
    static LootPoolEntry.Builder<?> itemEntryOf(Item item) {
        return ItemEntry.builder(item);
    }

    static LootContext newContext(Entity entity) {
        return lootContextCtor.newInstance(new Random(), 0F, (ServerWorld)null, (Function<?,?>)null, (Function<?,?>)null, ImmutableMap.of(LootContextParameters.ORIGIN, Vec3d.ZERO, LootContextParameters.THIS_ENTITY, entity), ImmutableMap.of(), null);
    }

    static LootTable.Builder tableBuilder() {
        return LootTable.builder();
    }
    static LootPool.Builder poolBuilder() {
        return LootPool.builder();
    }

    static NamedScreenHandlerFactory getScreenWithItems(List<ItemStack> items) {
        return new NamedScreenHandlerFactory() {
            @Override
            public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
                SimpleInventory inventory = new SimpleInventory(54);
                items.forEach(inventory::addStack);
                return new GenericContainerScreenHandler(ScreenHandlerType.GENERIC_9X6, syncId, inv, inventory, 6);
            }
            @Override
            public Text getDisplayName() {
                return LiteralText.EMPTY;
            }
    
        };
    }
}
