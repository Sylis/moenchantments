package com.biom4st3r.moenchantments;

import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.item.v1.ItemTooltipCallback;

import biom4st3r.libs.moenchant_lib.EnchantmentSkeleton;
import com.biom4st3r.moenchantments.logic.RetainedPotion;

@Environment(EnvType.CLIENT)
public class ClientEventHandlers {
    public static void init() {
        ItemTooltipCallback.EVENT.register((stack, context, tips) -> {
            RetainedPotion.borrowRetainedPotion(stack, rt -> {
                if (rt.getCharges() <= 0) return;
                for (int i = 0; i < tips.size(); i++) {
                    Text text = tips.get(i);
                    if (text instanceof TranslatableText) {
                        if (((TranslatableText)text).getKey().equals("enchantment.moenchantments.potionretension")) {
                            String name = new TranslatableText(rt.getEffect().getTranslationKey()).getString();
                            Text newText = new LiteralText(
                                String.format(
                                    "%s- %s %s%s %s/%s",
                                    Formatting.GRAY,
                                    name,
                                    EnchantmentSkeleton.toRoman(rt.getAmp()+1),
                                    Formatting.RESET,
                                    rt.getCharges(),
                                    rt.getMaxCharges()
                                    ));
                            tips.add(i+1, newText);
                        }
                    }
                }
            });
        });
    }
}
