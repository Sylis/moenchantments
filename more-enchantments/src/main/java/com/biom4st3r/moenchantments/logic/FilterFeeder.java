package com.biom4st3r.moenchantments.logic;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.util.Util;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import com.mojang.datafixers.util.Pair;

public class FilterFeeder {
    static Map<StatusEffect, StatusEffect> inverse = Util.make(Maps.newHashMap(), map -> {
        map.put(StatusEffects.POISON, StatusEffects.HEALTH_BOOST);
        map.put(StatusEffects.HUNGER, StatusEffects.ABSORPTION);
        map.put(StatusEffects.MINING_FATIGUE, StatusEffects.HASTE);
        map.put(StatusEffects.INSTANT_DAMAGE, StatusEffects.INSTANT_HEALTH);
        map.put(StatusEffects.BLINDNESS, StatusEffects.GLOWING);
        map.put(StatusEffects.BAD_OMEN, StatusEffects.HERO_OF_THE_VILLAGE);
        map.put(StatusEffects.NAUSEA, StatusEffects.NIGHT_VISION);
        map.put(StatusEffects.SLOWNESS, StatusEffects.SPEED);
        map.put(StatusEffects.WEAKNESS, StatusEffects.RESISTANCE);
        map.put(StatusEffects.UNLUCK, StatusEffects.LUCK);
        map.put(StatusEffects.WITHER, StatusEffects.REGENERATION);
    });
    public static FoodComponent createInverseFood(FoodComponent component) {
        FoodComponent.Builder food = new FoodComponent.Builder();
        food.saturationModifier(component.getSaturationModifier());
        food.hunger(component.getHunger());
        if (component.isAlwaysEdible()) food.alwaysEdible();
        if (component.isSnack()) food.snack();
        if (component.isMeat()) food.meat();
        for (Pair<StatusEffectInstance, Float> effectInstance : component.getStatusEffects()) {
            food.statusEffect(getInverse(effectInstance.getFirst()), effectInstance.getSecond());
        }
        return food.build();
    }

    static Queue<DUMMYFOODITEM> POOL = Queues.newArrayDeque();
    static ReentrantLock lock = new ReentrantLock();

    public static boolean hasValidItemAndServer(LivingEntity entity) {
        return !entity.world.isClient && EnchantmentRegistry.FILTER_FEEDER.hasEnchantment(entity.getEquippedStack(EquipmentSlot.HEAD));
    }
    public static boolean hasValidItem(Object obj) {
        LivingEntity entity = (LivingEntity) obj;
        return EnchantmentRegistry.FILTER_FEEDER.hasEnchantment(entity.getEquippedStack(EquipmentSlot.HEAD));
    }
    public static boolean hasValidItemAndServer(Object obj) {
        return hasValidItemAndServer((LivingEntity)obj);
    }
    private static DUMMYFOODITEM getObject(FoodComponent food) {
        FoodComponent inverse = createInverseFood(food);
        DUMMYFOODITEM pooledItem;
        lock.lock();
        if (!POOL.isEmpty()) {
            pooledItem = POOL.poll();
            lock.unlock();
        } else {
            lock.unlock();
            pooledItem = new DUMMYFOODITEM();
        }
        pooledItem.food = inverse;
        return pooledItem;
    }
    public static StatusEffectInstance getInverse(StatusEffectInstance instance) {
        return new StatusEffectInstance(inverse.getOrDefault(instance.getEffectType(), instance.getEffectType()), instance.getDuration(), instance.getAmplifier(), instance.isAmbient(), instance.shouldShowParticles(),instance.shouldShowIcon());
    }
    public static void borrowDummyItem(FoodComponent food, Consumer<Item> consumer) {
        DUMMYFOODITEM pooledItem = getObject(food);
        consumer.accept(pooledItem);
        lock.lock();
        POOL.add(pooledItem);
        lock.unlock();
    }
    public static DUMMYFOODITEM borrow(FoodComponent component) {
        return getObject(component);
    }

    public static class DUMMYFOODITEM extends Item {
        public FoodComponent food;
        public DUMMYFOODITEM() {
            super(new Item.Settings());
        }

        @Override
        public boolean isFood() {
            return true;
        }

        @Override
        public FoodComponent getFoodComponent() {
            return food;
        }
        public void release() {
            lock.lock();
            POOL.add(this);
            lock.unlock();
        }
    }
}
