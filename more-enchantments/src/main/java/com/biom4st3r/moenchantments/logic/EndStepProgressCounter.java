package com.biom4st3r.moenchantments.logic;

import java.util.ArrayList;
import java.util.Optional;

import com.biom4st3r.moenchantments.interfaces.EndStepper;
import com.biom4st3r.moenchantments.networking.Packets;
import com.google.common.collect.Lists;
import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import biom4st3r.libs.particle_emitter.ParticleEmitter;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Items;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.Vec3d;

public class EndStepProgressCounter {

    public enum Stage {
        START_DOWN,
        END_DOWN,
        END,
        ;
    }

    final LivingEntity target;
    private Stage stage = Stage.START_DOWN;
    final Vec3d start;
    final Vec3d end;
    int ticks = 0;
    long prev;
    ArrayList<Optional<ParticleEmitter>> emitters = Lists.newArrayList();
    /**
     * Used by EntityMixin to Block teleporting
     */
    private boolean canTeleport = false;


    public EndStepProgressCounter(LivingEntity target, Vec3d end) {
        if (target.world.isClient) {
            throw new IllegalAccessError("EndStepProgressCounters shouldn't be created on client");
        }
        this.target = target;
        target.noClip = true;
        target.setNoGravity(true);
        this.start = target.getPos();
        this.end = end;

        if (target.getType() == EntityType.PLAYER) Packets.SERVER.initEndStep((ServerPlayerEntity) target, this.write());
        this.emitters.addAll(EndStep.createEmitters(target.world, this.end));
        this.emitters.addAll(EndStep.createEmitters(target.world, this.start));
    }

    public boolean canTeleport() {
        return this.canTeleport;
    }

    public void update(Stage stage) {
        this.stage = stage;
    }

    private void updateStage(Stage stage) {
        this.stage = stage;
        Packets.SERVER.updateEndStepState((ServerPlayerEntity) this.target, stage);
    }

    @Environment(EnvType.CLIENT)
    public EndStepProgressCounter(PlayerEntity player, PacketByteBuf buf) {
        this(player, new Vec3d(buf.readDouble(), buf.readDouble(), buf.readDouble()), new Vec3d(buf.readDouble(), buf.readDouble(), buf.readDouble()), buf.readEnumConstant(Stage.class));
    }

    public PacketByteBuf write() {
        PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
        buf.writeDouble(this.start.x);
        buf.writeDouble(this.start.y);
        buf.writeDouble(this.start.z);
        buf.writeDouble(this.end.x);
        buf.writeDouble(this.end.y);
        buf.writeDouble(this.end.z);
        buf.writeEnumConstant(this.stage);
        return buf;
    }

    @Environment(EnvType.CLIENT)
    private EndStepProgressCounter(LivingEntity target, Vec3d start, Vec3d end, Stage stage) {
        this.target = target;
        this.start = start;
        this.end = end;
        this.stage = stage;
    }

    private static double normalizeStep(double step, long prevTime, long currTime) {
        if (prevTime == 0) {
            return 0;
        }
        double normal = step * ((currTime - prevTime) / 1E9D);
        return normal;
    }

    boolean hasBlinded = false;
    public void tick() {
        ticks++;
        switch(stage) {
            case END:
            this.requestTeleport(this.end);
                if (target.getType() == EntityType.PLAYER) {
                    ((PlayerEntity)target).getItemCooldownManager().set(Items.ENDER_PEARL, 25);
                }
                target.noClip = false;
                target.setNoGravity(false);
                target.resetPosition(end.x, end.y + 0.05, end.z);
                emitters.forEach(o ->{
                    o.ifPresent(p ->{
                        p.kill();
                    });
                });
                ((EndStepper)this.target).moenchantment$setStepProgress(Optional.empty());
                return;
            case END_DOWN:
                Vec3d pos = moveTowards(this.target.getPos(), this.end, normalizeStep(1.8D, prev, System.nanoTime()));
                this.target.resetPosition(pos.x, pos.y, pos.z);
                break;
            case START_DOWN:
                Vec3d pos0 = moveTowards(this.target.getPos(), this.start.subtract(0, 2, 0), normalizeStep(1.8D, prev, System.nanoTime()));
                this.target.resetPosition(pos0.x, pos0.y, pos0.z);
                break;
        }
        this.updatePreviousTime();
        if (!this.hasBlinded &&this.target.getPos().subtract(start.subtract(0, 2, 0)).length() < 1) {
            this.target.addStatusEffect(new StatusEffectInstance(StatusEffects.BLINDNESS, 30, 0, false, false, false));
        }
        if (this.target.world.isClient) {
            return;
        }

        if (stage == Stage.START_DOWN && this.target.getPos().equals(start.subtract(0, 2, 0))) {
            this.updateStage(Stage.END_DOWN);
            this.requestTeleport(end.subtract(0, 2, 0));
        } else if (this.target.getPos().equals(this.end)) {
            this.updateStage(Stage.END);
            this.requestTeleport(this.end);
            this.canTeleport = true;
        }
    }

    private void requestTeleport(Vec3d pos) {
        this.canTeleport = true;
        this.target.requestTeleport(pos.x, pos.y, pos.z);
        this.canTeleport = false;
    }

    private void updatePreviousTime() {
        this.prev = System.nanoTime();
    }

    public static Vec3d moveTowards(Vec3d current, Vec3d target, double maxDelta) {
        Vec3d delta = target.subtract(current);
        if (delta.length() <= maxDelta || delta.length() == 0.0D) {
            return target;
        }
        delta = delta.normalize();
        return  current.add(delta.multiply(maxDelta));
    }

    @Environment(EnvType.CLIENT)
    public void renderStartPortal() {

    }
    @Environment(EnvType.CLIENT)
    public void renderEndPortal() {

    }
}
