package com.biom4st3r.moenchantments.logic;

import java.util.function.Predicate;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.world.World;

public class EventCollection {
    public static void init() {
        AlphafireLogic.events();
        EnderProtectionLogic.events();
        EndStep.events();
        Familiarity.events();
        GrapnelLogic.events();
        PotionRetention.events();
        Slippery.events();
    }

    public static boolean checkBox(World world, Box box, Predicate<BlockState> condition) {
        boolean result = true;
        for (int x = (int) box.minX; x < box.maxX; x++) {
            for (int y = (int) box.minY; y < box.maxY; y++) {
                for (int z = (int) box.minZ; z < box.maxZ; z++) {
                    result &= condition.test(world.getBlockState(new BlockPos(x, y, z)));
                }
            }
        }
        return result;
    }
}
