package com.biom4st3r.moenchantments.logic;

import java.util.List;
import java.util.Map;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.MoEnchantsConfig;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;

import biom4st3r.libs.moenchant_lib.EnchantmentSkeleton;


/**
 * This is an attempt to add compat with other mods<p>
 * that modify the {@code PlayerInventory#dropAll()} logic<p>
 * to restore items after death.<p>
 * If you would like to see any changes made here feel free<p>
 * to @Biom4st3r in the fabricord or Submit an issue/PR<p>
 */
public final class SoulboundBindings {

    /**
     * Default behavior for an item that is about to be dropped from {@code PlayerInventory#dropAll()}<p>
     * Feel free to inject into or fully replace these variables
     */
    public static AttemptDropOnDeath ATTEMPT_DROP_BEHAVIOR = new AttemptDropOnDeath() {
        @Override
        public boolean shouldDropItemOnDeath(InventoryTarget target, ItemStack stack, List<ItemStack> sourceInventory,
                List<ItemStack> backupInventory, int inventoryIndex, PlayerInventory inventory) {
            if (EnchantmentSkeleton.hasEnchant(EnchantmentRegistry.SOULBOUND, stack)) {
                backupInventory.set(inventoryIndex, stack);
                return false;
            }
            return true;
        }
    };

    public static RestoreSoulboundItems ATTEMPT_RESTORE = new RestoreSoulboundItems(){

        @Override
        public void restore(InventoryTarget target, ItemStack stack,
                List<ItemStack> vanillaInventory, int inventoryIndex, PlayerInventory inventory) {
            vanillaInventory.set(inventoryIndex, stack);
            downgradeSoulBound(stack);
        }
    };

    public static PlayerEntityCopy COPY_SOULBOUND_ITEMS_ON_RESPAWN = new PlayerEntityCopy() {
        @Override
        public void copy(ServerPlayerEntity oldEntity, ServerPlayerEntity thisEntity, boolean isAlive) {
            if (!isAlive && EnchantmentRegistry.SOULBOUND.isEnabled() && !oldEntity.inventory.isEmpty()) {
                thisEntity.inventory.clone(oldEntity.inventory);
            }
        }
    };

    public static void downgradeSoulBound(ItemStack iS) {
        if (!MoEnchantsConfig.config.UseStandardSoulboundMechanics) {
            Map<Enchantment, Integer> enchantments = EnchantmentHelper.get(iS);
            int soulboundLvl = EnchantmentRegistry.SOULBOUND.getLevel(iS);
            iS.removeSubTag("Enchantments");
            enchantments.remove(EnchantmentRegistry.SOULBOUND);
            for (Enchantment ench : enchantments.keySet()) {
                iS.addEnchantment(ench, enchantments.get(ench));
            }
            if (soulboundLvl > 1) {
                iS.addEnchantment(EnchantmentRegistry.SOULBOUND, soulboundLvl-1);
            }
        }
    }

    public static enum InventoryTarget {
        MAIN,
        ARMOR,
        OFFHAND,
        UNKNOWN
    }

    public static interface PlayerEntityCopy
    {
        void copy(ServerPlayerEntity oldEntity, ServerPlayerEntity thisEntity, boolean isAlive);
    }

    public static interface RestoreSoulboundItems
    {
        /**
         * 
         * @param target
         * @param stack
         * @param vanillaInventory
         * @param inventoryIndex
         * @param inventory
         */
        void restore(InventoryTarget target, ItemStack stack, List<ItemStack> vanillaInventory, int inventoryIndex, PlayerInventory inventory);
    }

    public static interface AttemptDropOnDeath
    {
        /**
         * If the return value is false the call to {@code List#get(int)} in<p> 
         * {@code PlayerInventory#dropAll()} will replaced with ItemStack.EMPTY <p> 
         * thus shorting out {@code PlayerEntity#dropItem(ItemStack, boolean, boolean)}
         * @param target
         * @param stack the item in question
         * @param sourceInventory the vanilla inventory main, armor, or offhand
         * @param backupInventory the list used to store items that should be restored
         * @param inventoryIndex currentIndex in (@code stack)
         * @param inventory the PlayerInventory
         * @return
         */
        boolean shouldDropItemOnDeath(InventoryTarget target, ItemStack stack, List<ItemStack> sourceInventory, List<ItemStack> backupInventory, int inventoryIndex, PlayerInventory inventory);
    }

}
