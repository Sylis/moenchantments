package com.biom4st3r.moenchantments.logic;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.MoEnchantsConfig;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

/**
 * Object Pool
 */
public class RetainedPotion implements Closeable {

    private RetainedPotion() {}
    private static final String KEY = "biom4st3rpotionretainer";
    private static final ReentrantLock LOCK = new ReentrantLock();
    private static final Queue<RetainedPotion> POOL = new ArrayDeque<>();
    private static void lockPool() {
        LOCK.lock();
    }
    private static void releasePool() {
        LOCK.unlock();
    }
    private static RetainedPotion getObject(ItemStack stack) {
        RetainedPotion rt;
        lockPool();
        if (!POOL.isEmpty()) {
            rt = POOL.poll();
            releasePool();
        } else {
            releasePool();
            rt = new RetainedPotion();
        }

        rt.stack = stack;

        return rt;
    }

    public boolean isEmpty() {
        return this.effect == null;
    }

    StatusEffect effect = null;
    int amp = -1;
    int charges = -1;
    ItemStack stack = ItemStack.EMPTY;

    public void setPotionEffectAndCharges(StatusEffectInstance effect, int val) {
        if (this.effect == null) {
            this.effect = effect.getEffectType();
            this.amp = effect.getAmplifier();
            this.charges = Math.min(val, this.getMaxCharges());
        } else if (this.effect == effect.getEffectType()) {
            this.addCharges(val);
        }
    }

    public int getMaxCharges() {
        return EnchantmentRegistry.POTIONRETENTION.getLevel(this.stack) * MoEnchantsConfig.config.perLevelChargeMultiplierForPotionRetention; 
    }

    public void toTag(CompoundTag tag) {
        CompoundTag ct = new CompoundTag();
        if (effect != null) {
            ct.putString("effect", Registry.STATUS_EFFECT.getId(effect).toString());
            ct.putInt("amp", amp);
            ct.putInt("charges", charges);
        }
        tag.put(KEY, ct);
    }

    public StatusEffect getEffect() {
        return effect;
    }
    public int getCharges() {
        return charges;
    }
    public int getAmp() {
        return this.amp;
    }

    public void addCharges(int val) {
        this.charges = Math.min(this.getMaxCharges(), val + charges);
        if (this.charges <= 0) {
            this.erase();
        }
    }

    public void useCharge(LivingEntity entity) {
        if (this.effect != null) {
            StatusEffectInstance instance = new StatusEffectInstance(effect, 5*20, this.amp);
            this.addCharges(-1);
            PotionRetention.applyRetainedPotionEffect(entity, instance);
        }
    }

    public static boolean borrowFirstRetainedPotion(Consumer<RetainedPotion> consumer, ItemStack... stacks) {
        for (ItemStack stack : stacks) {
            if (EnchantmentRegistry.POTIONRETENTION.hasEnchantment(stack)) {
                RetainedPotion rt = RetainedPotion.fromStack(stack);
                consumer.accept(rt);
                rt.applyAndRelease();
                return true;
            }
        }
        return false;
    }

    public static boolean borrowRetainedPotion(ItemStack stack, Consumer<RetainedPotion> consumer) {
        if (!EnchantmentRegistry.POTIONRETENTION.hasEnchantment(stack)) {
            return false;
        }
        RetainedPotion rt = RetainedPotion.fromStack(stack);
        consumer.accept(rt);
        rt.applyAndRelease();
        return true;
    }

    private static RetainedPotion fromStack(ItemStack stack) {
        RetainedPotion rt = getObject(stack);
        if (stack.getSubTag(KEY) != null) {
            CompoundTag ct = stack.getTag().getCompound(KEY);
            rt.effect = Registry.STATUS_EFFECT.get(new Identifier(ct.getString("effect")));
            rt.amp = ct.getInt("amp");
            rt.charges = ct.getInt("charges");
        }
        return rt;
    }

    public void erase() {
        this.effect = null;
        this.amp = -1;
        this.charges = -1;
    }

    public void applyAndRelease() {
        this.toTag(stack.getOrCreateTag());
        this.erase();
        this.stack = ItemStack.EMPTY;
        lockPool();
        POOL.add(this);
        releasePool();
    }
    @Override
    public void close() throws IOException {
        this.applyAndRelease();
    }
}
