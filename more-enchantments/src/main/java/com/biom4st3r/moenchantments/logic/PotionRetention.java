package com.biom4st3r.moenchantments.logic;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CauldronBlock;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.TypedActionResult;

import net.fabricmc.fabric.api.event.player.UseBlockCallback;
import net.fabricmc.fabric.api.event.player.UseItemCallback;

import biom4st3r.libs.moenchant_lib.events.LivingEntityDamageEvent;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.mixin.potion_retention.PotionRetentionAccessor;

public class PotionRetention {
    public static void events() {
        // Potion Retention clean weapon
        UseBlockCallback.EVENT.register((player, world, hand, hitResult) -> {
            BlockState state = world.getBlockState(hitResult.getBlockPos());
            if (state.getBlock() == Blocks.CAULDRON && state.get(CauldronBlock.LEVEL) > 0) {
                return RetainedPotion.borrowRetainedPotion(player.getStackInHand(hand), rt -> {
                    rt.erase();
                }) ? ActionResult.SUCCESS : ActionResult.PASS;
            }
            return ActionResult.PASS;
        });
        // Potion Retention apply retained effect
        UseItemCallback.EVENT.register((player, world, hand) -> {
            ItemStack stack = player.getStackInHand(hand);
            if (!world.isClient) {
                return RetainedPotion.borrowRetainedPotion(stack, rt -> {
                    rt.useCharge(player);
                }) ? new TypedActionResult<ItemStack>(ActionResult.SUCCESS, stack) : new TypedActionResult<ItemStack>(ActionResult.PASS, stack);
            }
            return new TypedActionResult<ItemStack>(ActionResult.PASS, stack);
        });
        // Potion Retention apply to attack target
        LivingEntityDamageEvent.EVENT.register((source, damage, target) -> {
            if (source.getAttacker() instanceof LivingEntity) {
                LivingEntity attacker = (LivingEntity) source.getAttacker();
                RetainedPotion.borrowRetainedPotion(attacker.getMainHandStack(), rt -> {
                    rt.useCharge((LivingEntity) target);
                });
            }
            return LivingEntityDamageEvent.PASS;
        });
    }
    public static boolean applyRetainedPotionEffect(LivingEntity entity, StatusEffectInstance newStatusEffect) {
        PotionRetentionAccessor target = (PotionRetentionAccessor) entity;
        if (!target.invokeCanHaveStatusEffect(newStatusEffect)) {
            return false;
        } else {
            StatusEffectInstance foundStatusEffect = (StatusEffectInstance) target.getActiveStatusEffects()
                    .get(newStatusEffect.getEffectType());
            if (foundStatusEffect == null) { // if not in there
                target.getActiveStatusEffects().put(newStatusEffect.getEffectType(), newStatusEffect);
                target.invokeOnStatusEffectApplied(newStatusEffect);
                ModInit.logger.debug("adding new effect to activestatuseffects Map");
                return true;
            } else if (foundStatusEffect.getEffectType() == newStatusEffect.getEffectType()
                    && foundStatusEffect.getAmplifier() == newStatusEffect.getAmplifier()) { // if has instance of
                foundStatusEffect = new StatusEffectInstance(foundStatusEffect.getEffectType(),
                        foundStatusEffect.getDuration() + newStatusEffect.getDuration(),
                        foundStatusEffect.getAmplifier());
                target.getActiveStatusEffects().replace(foundStatusEffect.getEffectType(), foundStatusEffect);
                target.invokeOnStatusEffectApplied(foundStatusEffect);
                ModInit.logger.debug("increased duration of effect in activestatuseffects map");
                return true;
            } else {
                return false;
            }
        }
    }
}
