package com.biom4st3r.moenchantments.logic;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.biom4st3r.moenchantments.MoEnchantsConfig;
import com.biom4st3r.moenchantments.ModInit;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;

import net.minecraft.block.Block;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.entry.AlternativeEntry;
import net.minecraft.loot.entry.LeafEntry;
import net.minecraft.loot.entry.LootPoolEntry;
import net.minecraft.loot.entry.LootPoolEntryTypes;
import net.minecraft.loot.function.ApplyBonusLootFunction;
import net.minecraft.loot.function.ApplyBonusLootFunction.OreDrops;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.RecipeType;
import net.minecraft.recipe.SmeltingRecipe;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

/**
 * AlphafireLogic
 */
public class AlphafireLogic {

    public static Set<Block> blocks_without_loot_function = Sets.newHashSet();
    public static List<Item> autoSmelt_blacklist = Lists.newArrayList();

    public static void events() {
        LootTableLoadingCallback.EVENT.register((resourceManager, manager, id, supplier, setter)  ->  {
            if (!MoEnchantsConfig.config.ApplyLootingToAlphaFire)
                return;
            String[] path = id.getPath().split("/");
            for (String x : MoEnchantsConfig.config.veinMinerBlockWhiteList) {
                if ((path.length == 2 ? path[1] : path[0]).contains(x.split(":")[1]) && id.getNamespace() == x.split(":")[0]) {
                    LootTable lt = manager.getTable(id);
                    // I'm sorry. I need labels here
                    label0: for (LootPool pool : lt.pools) {
                        // Oh God
                        for (LootPoolEntry entry : pool.entries) {
                            if (entry.getType() == LootPoolEntryTypes.ALTERNATIVES) { // Please
                                for (LootPoolEntry lpe : ((AlternativeEntry) entry).children) {
                                    for (LootFunction function : ((LeafEntry) lpe).functions) { // Make it stop
                                        if (function instanceof ApplyBonusLootFunction
                                                && ((ApplyBonusLootFunction) function).enchantment == Enchantments.FORTUNE) {
                                            Registry.BLOCK.getOrEmpty(new Identifier(x))
                                                    .ifPresent(b  ->  AlphafireLogic.blocks_without_loot_function.remove(b));
                                            break label0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    static Set<Item> items_without_fortune = Sets.newHashSet();

    private static OreDrops formula = new OreDrops();

    public static void storeExp(ItemStack stack, float value) {
        float stored = stack.getOrCreateTag().getFloat("alphafireStoredExp");
        stored+=value;
        stack.getOrCreateTag().putFloat("alphafireStoredExp", stored);
    }
    public static int removeExp(ItemStack stack) {
        float stored = stack.getOrCreateTag().getFloat("alphafireStoredExp");
        int toReturn = (int) Math.floor(stored);
        stack.getOrCreateTag().putFloat("alphafireStoredExp", stored-toReturn);
        return toReturn;
    }

    public static void attemptSmeltStacks(ServerWorld world, Entity entity, ItemStack tool, List<ItemStack> dropList) {
        if (items_without_fortune.isEmpty() && MoEnchantsConfig.config.ApplyLootingToAlphaFire) {
            items_without_fortune = AlphafireLogic.blocks_without_loot_function.stream().map(b -> b.asItem()).peek(i -> ModInit.logger.debug(i.toString())).collect(Collectors.toSet());
            AlphafireLogic.blocks_without_loot_function = null;
        }
        RecipeManager rm = world.getRecipeManager(); 
        Inventory basicInv = new SimpleInventory(1);

        ItemStack itemToBeChecked = ItemStack.EMPTY;
        Optional<SmeltingRecipe> smeltingResult;

        for (int stacksIndex = 0; stacksIndex < dropList.size(); stacksIndex++) {
            itemToBeChecked = dropList.get(stacksIndex);
            basicInv.setStack(0, itemToBeChecked);
            smeltingResult = rm.getFirstMatch(RecipeType.SMELTING, basicInv, entity.world);
            if (smeltingResult.isPresent() && !(AlphafireLogic.autoSmelt_blacklist.contains(itemToBeChecked.getItem()))) {
                int count = itemToBeChecked.getCount();
                if (items_without_fortune.contains(itemToBeChecked.getItem()) && EnchantmentHelper.getLevel(Enchantments.FORTUNE, tool) > 0) {
                    count = formula.getValue(world.getRandom(), count, EnchantmentHelper.getLevel(Enchantments.FORTUNE, tool));
                    if (MoEnchantsConfig.config.AlphaFireCauseExtraDamage &&  entity instanceof PlayerEntity) {
                        tool.damage(Math.max(count-1, 3), (PlayerEntity)entity,(playerEntity_1)  ->  {
                            playerEntity_1.sendToolBreakStatus(((PlayerEntity)entity).getActiveHand());
                        });
                    }
                }
                dropList.set(stacksIndex, new ItemStack(smeltingResult.get().getOutput().getItem(),count));
                AlphafireLogic.storeExp(tool, smeltingResult.get().getExperience() * itemToBeChecked.getCount());
                // ((PlayerExperienceStore)(Object)entity).biom4st3r_addExp(smeltingResult.get().getExperience() * itemToBeChecked.getCount());
                if (!ModInit.lockAutoSmeltSound) {
                    ModInit.lockAutoSmeltSound = true;
                    world.playSound((PlayerEntity)null, entity.getBlockPos(), SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.1f, 0.1f);
                }
            }
        }
    }
}