package com.biom4st3r.moenchantments.logic;

import java.util.UUID;

import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;

import biom4st3r.libs.moenchant_lib.EnchantmentSkeleton;
import biom4st3r.libs.moenchant_lib.events.LivingEntityDamageEvent;
import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.MoEnchantsConfig;

public class Familiarity {
    
    public static final UUID uuidZero = new UUID(0L, 0L);

    public static void events() {
        LivingEntityDamageEvent.EVENT.register((damageSource, damage, le)  ->  {
            if (le instanceof TameableEntity) {
               TameableEntity defender = (TameableEntity) le;
               if (damageSource.getAttacker() instanceof PlayerEntity && defender.getOwnerUuid() != Familiarity.uuidZero) {
                   PlayerEntity attacker = (PlayerEntity) damageSource.getAttacker();
                   if (EnchantmentSkeleton.hasEnchant(EnchantmentRegistry.TAMEDPROTECTION, attacker.getMainHandStack())) {
                       if (MoEnchantsConfig.config.TameProtectsOnlyYourAnimals) {
                           if (defender.isOwner(attacker)) {
                               return LivingEntityDamageEvent.FAIL;
                           }
                       } else {
                           return LivingEntityDamageEvent.FAIL;
                       }
                   }
               }
           }
           return LivingEntityDamageEvent.PASS;
       });
    }
}
