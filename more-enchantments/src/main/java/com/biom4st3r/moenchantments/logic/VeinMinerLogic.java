package com.biom4st3r.moenchantments.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Queue;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.tag.BlockTags;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.MoEnchantsConfig;
import com.biom4st3r.moenchantments.ModInit;
import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;

public final class VeinMinerLogic {

    public static IntArrayList int_block_whitelist = new IntArrayList(100);

    public static void tryVeinMining(World world, BlockPos blockPos, BlockState blockState, PlayerEntity pe) {
        if (world.isClient) return;
        if (pe==null) return; // Patch for Adorn CarpetedBlock.
        if (!pe.isUsingEffectiveTool(blockState)) return; // Slippery support
        
        ItemStack tool = pe.getMainHandStack();
        if (tool == ItemStack.EMPTY || !tool.hasEnchantments()) {
            return;
        }
        int veinMinerLvl = EnchantmentRegistry.VEINMINER.getLevel(tool);
        int treeFellerLvl = EnchantmentRegistry.TREEFELLER.getLevel(tool);
        boolean hasAutoSmelt = EnchantmentRegistry.AUTOSMELT.hasEnchantment(tool);

        Block currentType = blockState.getBlock();
        int brokenBlocks = 0;

        if (treeFellerLvl > 0 && isValidBlockForAxe(blockState)) {
            brokenBlocks = doVeinMiner(blockState, world, blockPos, MoEnchantsConfig.config.TreeFellerMaxBreakByLvl[treeFellerLvl-1], pe, tool);
            ModInit.logger.debug("%s block broken",brokenBlocks);
        }
        else if (veinMinerLvl > 0 && isValidBlockForPick(blockState)) {
            brokenBlocks = doVeinMiner(blockState, world, blockPos, MoEnchantsConfig.config.VeinMinerMaxBreakByLvl[veinMinerLvl-1], pe, tool);
            ModInit.logger.debug("%s block broken",brokenBlocks);
        }
        pe.increaseStat(Stats.MINED.getOrCreateStat(currentType), brokenBlocks);
        if (hasAutoSmelt) {
            int retrievedXp = AlphafireLogic.removeExp(tool);
            ModInit.logger.debug("retrieved XP %s", retrievedXp);
            for (; retrievedXp > 0; retrievedXp--) {
                dropExperience(1, blockPos, world);
            }
        }
    }

    private static boolean isValidBlockForPick(BlockState state) {
        if (VeinMinerLogic.int_block_whitelist.contains(Registry.BLOCK.getRawId(state.getBlock()))) {
            return true;
        }
        ModInit.logger.debug("%s global false",state.toString());
        return false;
        
    }

    private static boolean isValidBlockForAxe(BlockState state) {
        if (state.getMaterial() == Material.WOOD) return true;
        if (BlockTags.LOGS.contains(state.getBlock())) return true;
        return false;
    }
    
    private static void dropExperience(float experienceValue ,BlockPos blockPos, World world) {
        world.spawnEntity(new ExperienceOrbEntity(world, blockPos.getX(), blockPos.getY(), blockPos.getZ(), (int)experienceValue));
    }

    public static ArrayList<BlockPos> getSameBlocks(World w, BlockPos source, Block type) {
        ArrayList<BlockPos> t = Lists.newArrayList();
        BlockPos[] poses = new BlockPos[] {
            source.down().north(),source.down().east(),source.down().west(),source.down().south(),
            source.up(),source.down(),source.north(),source.east(),source.south(),source.west(),
            source.up().south(),source.up().east(),source.up().west(),source.up().north()};
        
        for (BlockPos pos : poses) {
            if (w.getBlockState(pos).getBlock() == type) {
                t.add(pos);
            }
        }
        Collections.shuffle(t);
        return t;
    }

    private static int doVeinMiner(BlockState blockState, World world, BlockPos blockPos, int maxBlocks, PlayerEntity pe, ItemStack tool) {
        ModInit.logger.debug("BlockType %s", blockState.getBlock());
        // int blocksBroken = 0;
        LongOpenHashSet used = new LongOpenHashSet(29);
        Queue<BlockPos> toBreak = Util.make(Queues.newArrayBlockingQueue(29), queue -> queue.add(blockPos));

        while(!toBreak.isEmpty() && used.size() <= (maxBlocks)) {
            BlockPos currPos = toBreak.poll();
            BlockState currentState = world.getBlockState(currPos);
            used.add(currPos.asLong());
            if (toBreak.size() < 50) {
                getSameBlocks(world, currPos, blockState.getBlock())
                    .stream()
                    .filter(pos -> !used.contains(pos.asLong()))
                    .forEach(toBreak::offer);
                    ;
            }
            // May not be compatible with protection mods
            Block.dropStacks(currentState, world, blockPos, null, pe, tool);
            world.breakBlock(currPos, false);
            currentState.getBlock().onBreak(world, currPos, currentState, pe);
            tool.postMine(world, currentState, currPos, pe);
            
            tool.damage(1, pe,(playerEntity_1)  ->  {
                playerEntity_1.sendToolBreakStatus(pe.getActiveHand());
            });
            if (MoEnchantsConfig.config.ProtectItemFromBreaking ? tool.getMaxDamage()-tool.getDamage() < 5 : false) {
                pe.playSound(SoundEvents.ENTITY_ITEM_BREAK, SoundCategory.PLAYERS, 1f, 1f);
                pe.sendMessage(new TranslatableText("dialog.moenchantments.tool_damage_warning_veinminer").formatted(Formatting.GREEN),false);
                break;
            }
        }
        ModInit.lockAutoSmeltSound = false;
        pe.addExhaustion(0.005F * used.size());
        return used.size();
    }

}