package com.biom4st3r.moenchantments;

import java.util.function.Predicate;

import net.minecraft.enchantment.Enchantment.Rarity;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.projectile.PersistentProjectileEntity.PickupPermission;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.AxeItem;
import net.minecraft.item.BowItem;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.MiningToolItem;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.SwordItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import biom4st3r.libs.moenchant_lib.EnchantBuilder;
import biom4st3r.libs.moenchant_lib.EnchantmentSkeleton;
import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import com.biom4st3r.moenchantments.logic.ChaosArrowLogic;

/**
 * EnchantmentRegistry
 * Centeral store for Enchantment instances and registry
 */
public class EnchantmentRegistry {
    public static final String MODID = ModInit.MODID;
    public static final EquipmentSlot[] 
        ARMOR_SLOTS = new EquipmentSlot[] {EquipmentSlot.CHEST, EquipmentSlot.FEET, EquipmentSlot.HEAD, EquipmentSlot.LEGS},
        ALL_SLOTS = new EquipmentSlot[] {EquipmentSlot.CHEST, EquipmentSlot.FEET, EquipmentSlot.HEAD, EquipmentSlot.LEGS, EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND},
        HAND_SLOTS = new EquipmentSlot[] {EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND};

    static Predicate<ItemStack> 
        isArmor = stack -> (stack.getItem() instanceof ArmorItem),
        isChest = stack -> isArmor.test(stack) && ((ArmorItem)stack.getItem()).getSlotType() == EquipmentSlot.CHEST,
        isShoe = stack -> isArmor.test(stack) && ((ArmorItem)stack.getItem()).getSlotType() == EquipmentSlot.FEET,
        isPant = stack -> isArmor.test(stack) && ((ArmorItem)stack.getItem()).getSlotType() == EquipmentSlot.LEGS,
        isHat = stack -> isArmor.test(stack) && ((ArmorItem)stack.getItem()).getSlotType() == EquipmentSlot.HEAD,
        isTool = stack -> stack.getItem().isDamageable() && !isArmor.test(stack),
        isPickaxe = stack -> stack.getItem() instanceof PickaxeItem,
        isAxe = stack -> stack.getItem() instanceof AxeItem,
        isMiningTool = stack -> stack.getItem() instanceof MiningToolItem,
        isSword = stack -> stack.getItem() instanceof SwordItem,
        YES = stack -> true,
        NO = YES.negate(),
        isBow = stack -> stack.getItem() instanceof BowItem,
        isCrossbow = stack -> stack.getItem() instanceof CrossbowItem
        ;

    // 1 5 - 35
    // 2 10 - 40
    // 3 15 - 45
    public static final EnchantmentSkeleton TREEFELLER = new EnchantBuilder(Rarity.UNCOMMON, EquipmentSlot.MAINHAND)
        .maxlevel(MoEnchantsConfig.config.TreeFellerMaxBreakByLvl.length)
        .isAcceptible(isAxe)
        .minpower(level -> level*5)
        .enabled(true)
        .build("treefeller")
        .register();
    // 1 5 - 35
    // 2 10 - 40
    // 3 15 - 45
    public static final EnchantmentSkeleton VEINMINER = new EnchantBuilder(Rarity.UNCOMMON, EquipmentSlot.MAINHAND)
        .maxlevel(MoEnchantsConfig.config.VeinMinerMaxBreakByLvl.length)
        .isAcceptible(isPickaxe)
        .minpower(level -> level*5)
        .maxpower(level -> level*10)
        .enabled(true)
        .build("veinminer")
        .register();
    // 1 30 - 50
    public static final EnchantmentSkeleton AUTOSMELT = new EnchantBuilder(Rarity.VERY_RARE, EquipmentSlot.MAINHAND)
        .addExclusive(Enchantments.SILK_TOUCH)
        .enabled(true)
        .isAcceptible(isMiningTool)
        .addExclusive(Enchantments.SILK_TOUCH)
        .minpower(level -> 30)
        .maxpower(leve -> 50)
        .build("autosmelt")
        .register();
    // 1 5 - 35
    public static final EnchantmentSkeleton TAMEDPROTECTION = new EnchantBuilder(Rarity.COMMON, EquipmentSlot.MAINHAND)
        .isAcceptible(isSword.or(isAxe).or(isBow).or(isCrossbow))
        .treasure(true)
        .minpower(level -> 5)
        .enabled(true)
        .build("tamedprotection")
        .register();
    // 1 1
    // 2 20 - 50
    // 3 30 - 60
    public static final EnchantmentSkeleton ENDERPROTECTION = new EnchantBuilder(Rarity.VERY_RARE, ARMOR_SLOTS)
        .maxlevel(3)
        .minpower(level -> (level)*10)
        .maxpower(level ->  (level)*11)
        .isAcceptibleInAnvil((is, ash) ->  false)
        .isAcceptible(isArmor)
        .setApplicationLogic((power, stack, treasureAllowed, list) ->  {
            Enchantment enchant = Registry.ENCHANTMENT.get(new Identifier(MODID, "curseofender"));
            if (treasureAllowed != enchant.isTreasure()) return;
            if (enchant.isAcceptableItem(stack) || (stack.getItem() == Items.BOOK && ExtendedEnchantment.cast(enchant).isAcceptibleOnBook())) {
                for (int lvl = enchant.getMaxLevel(); lvl > enchant.getMinLevel() - 1; --lvl) {
                    if (power >= enchant.getMinPower(lvl) && power <= enchant.getMaxPower(lvl)) {
                        list.add(new EnchantmentLevelEntry(enchant, lvl == 3 ? 1 : lvl == 1 ? 3 : 2));
                        break;
                    }
                }
            }
        })
        .treasure(true)
        .curse(true)
        .enabled(true)
        .build("curseofender");
    //Gitlab @Mr Cloud
    // 1 21 - 51
    public static final EnchantmentSkeleton SOULBOUND = new EnchantBuilder(Rarity.UNCOMMON, ALL_SLOTS)
        .enabled(MoEnchantsConfig.config.EnableSoulbound)
        .maxlevel(MoEnchantsConfig.config.UseStandardSoulboundMechanics ? 1 : 10)
        .minpower(level -> 13 * level)
        .maxpower(level -> (int)(14.4 * level))
        .isAcceptible(YES)
        .treasure(true)
        .build("soulbound")
        .register();
    public static final EnchantmentSkeleton POTIONRETENTION = new EnchantBuilder(Rarity.RARE, HAND_SLOTS)
        .minpower(level -> 7*level)
        .maxlevel(10)
        .isAcceptible(isSword.or(isAxe))
        .enabled(true)
        .build("potionretension")
        .register();
    
    // someone responding to Jeb_ on reddit.
    // 1 10 - 40
    // 2 20 - 50
    public static final EnchantmentSkeleton BOW_ACCURACY = new EnchantBuilder(Rarity.RARE, HAND_SLOTS)
        .maxlevel(2)
        .applyEnchantmentToArrows()
        .minpower(level -> level*10)
        .enabled(!ModInit.extraBowsFound)
        .isAcceptible(isBow.or(isCrossbow))
        .build("bowaccuracy")
        .register();
    public static final EnchantmentSkeleton BOW_ACCURACY_CURSE = new EnchantBuilder(Rarity.RARE, HAND_SLOTS)
        .maxlevel(1)
        .curse(true)
        .applyEnchantmentToArrows()
        .minpower(l -> 20)
        .maxpower(l -> 60)
        .addExclusive(BOW_ACCURACY)
        .isAcceptible(isBow.or(isCrossbow))
        .enabled(!ModInit.extraBowsFound)
        .build("bowinaccuracy")
        .register();
    // Gitlab @cryum
    //1 30 - 60
    public static final EnchantmentSkeleton ARROW_CHAOS = new EnchantBuilder(Rarity.VERY_RARE, HAND_SLOTS)
        .curse(true)
        .isAcceptible(isBow.or(isCrossbow))
        .onArrowCreated((arrow,weapon,arrowStack,shooter)-> {
            if(ChaosArrowLogic.makePotionArrow(shooter, arrow, shooter.getRandom())) {
                arrow.pickupType = PickupPermission.CREATIVE_ONLY;
            }
        })
        .minpower(level -> 30)
        .enabled(!ModInit.extraBowsFound)
        .build("arrow_chaos");
    // cryum from issue #13
    // 1 16 - 46
    // 2 32 - 62
    public static final EnchantmentSkeleton GRAPNEL = new EnchantBuilder(Rarity.RARE, HAND_SLOTS)
        .minpower(level -> 20)
        .maxlevel(1)
        .treasure(true)
        .enabled(true)
        .applyEnchantmentToArrows()
        .isAcceptible(isBow.or(isCrossbow))
        .addExclusive(Enchantments.MULTISHOT)
        .addExclusive(ARROW_CHAOS)
        .build("grapnel")
        .register();
    // Pulls all nearby entity towards the landing arrow. ?Impodes/deals massive damage to hit target?
    static final EnchantmentSkeleton IMPLODE = new EnchantBuilder(Rarity.RARE, HAND_SLOTS)
        .maxlevel(1)
        .isAcceptible(isBow.or(isCrossbow))
        .minpower(lvl ->22)
        .maxpower(lvl ->27)
        .addExclusive(GRAPNEL)
        .treasure(true)
        .enabled(false)
        .build("arrow_implode");
    // KORR Issue #48
    // ChestPlate less air underwater
    static final EnchantmentSkeleton DISCOMFORT = new EnchantBuilder(Rarity.RARE, EquipmentSlot.CHEST)
        .maxlevel(1)
        .curse(true)
        .isAcceptible(isChest)
        .treasure(true)
        .minpower(lvl ->12)
        .maxpower(lvl ->20)
        .enabled(false)
        .build("discomfort")
        .register();
    // KORR Issue #48
    // Chance to knock an item our of a mob/players hand
    static final EnchantmentSkeleton DISARM = new EnchantBuilder(Rarity.UNCOMMON, HAND_SLOTS)
        .maxlevel(1)
        .isAcceptible(isTool)
        .enabled(false)
        .build("disarm")
        .register();
    // When being repaired in any capcity there is a chance to remove a random enchantment including itself
    // KORR Issue #48 Items
    static final EnchantmentSkeleton DISENCHANTMENT = new EnchantBuilder(Rarity.VERY_RARE, ALL_SLOTS)
        .maxlevel(3) // Weakly Magical, Barely Magical, Not Magical
        .isAcceptible(YES)
        .enabled(false)
        .build("disenchantment")
        .register();
    // RM Issue #33
    // When a block is broke it goes straight into your inventory.
    public static final EnchantmentSkeleton BLACK_HOLE = new EnchantBuilder(Rarity.VERY_RARE, ALL_SLOTS)
        .maxlevel(1)
        .isAcceptible(isMiningTool)
        .minpower(lvl ->30)
        .maxpower(lvl ->37)
        .treasure(true)
        .enabled(true)
        .build("blackhole")
        .register();
    // KORR Issue #48
    // When use or hit and the item you have a chance to drop it
    public static final EnchantmentSkeleton SLIPPERY = new EnchantBuilder(Rarity.VERY_RARE, HAND_SLOTS)
        .maxlevel(1)
        .isAcceptible(ItemStack::isDamageable)
        .treasure(true)
        .curse(true)
        .minpower(lvl -> 1)
        .maxpower(lvl -> 40)
        .enabled(true)
        .build("slippery")
        .register();
    // When using ender pearls instead of throwing the pearl you go to where you're looking
    public static final EnchantmentSkeleton END_BOOTS = new EnchantBuilder(Rarity.VERY_RARE, EquipmentSlot.FEET)
        .maxlevel(1)
        .isAcceptible(isShoe)
        .treasure(true)
        .enabled(true) //FabricLoader.getInstance().isDevelopmentEnvironment()
        .build("voidstep")
        .register();
    // Eating things that would hurt you now help. Posion Potatos, Spider eyes
    public static final EnchantmentSkeleton FILTER_FEEDER = new EnchantBuilder(Rarity.VERY_RARE, EquipmentSlot.HEAD)
        .maxlevel(1)
        .isAcceptible(isHat)
        .treasure(true)
        .enabled(true)
        .build("filterfeeder")
        .register();
    // Full density fully reflects arrows and Tridents. Higher levels increase the chance of damaging attacking weapons or dropping the attacking item
    public static final EnchantmentSkeleton DENSITY = new EnchantBuilder(Rarity.VERY_RARE, ARMOR_SLOTS)
        .maxlevel(1)
        .isAcceptible(isArmor)
        .treasure(true)
        .enabled(false)
        .build("densearmor")
        .register();
    // Can eat when full
    public static final EnchantmentSkeleton GLUTEN = new EnchantBuilder(Rarity.UNCOMMON, EquipmentSlot.CHEST)
        .maxlevel(1)
        .isAcceptible(isChest)
        .treasure(true)
        .enabled(false)
        // .addExclusive(DISCOMFORT) // a chestplate can't be constrictive and expansive
        .build("gluten")
        .register();
    public static void classLoad() {
    }
}