package com.biom4st3r.moenchantments.mixin;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerInteractionManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.logic.Slippery;
import com.biom4st3r.moenchantments.logic.VeinMinerLogic;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ServerPlayerInteractionManager.class)
public abstract class OnBlockBreakAttemptMxn {

    @Inject(at = @At(shift = Shift.BEFORE ,value = "INVOKE", target = "net/minecraft/server/world/ServerWorld.removeBlock(Lnet/minecraft/util/math/BlockPos;Z)Z"), method = "tryBreakBlock")
    public void biom4st3r_attempVeinMine(BlockPos pos, CallbackInfoReturnable<Boolean> ci) {
        PlayerEntity pe = ((ServerPlayerInteractionManager)(Object)this).player;
        World world = pe.getEntityWorld();
        if (EnchantmentRegistry.SLIPPERY.hasEnchantment(pe.getMainHandStack())) {
            if (Slippery.shouldDropOnMine(world.random)) {
                Slippery.doDrop(pe);
            }
        }
        VeinMinerLogic.tryVeinMining(world, pos, world.getBlockState(pos), pe);
    }
}