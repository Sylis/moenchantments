package com.biom4st3r.moenchantments.mixin.potion_retention;

import java.util.Map;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.gen.Invoker;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;

@Mixin({LivingEntity.class})
public interface PotionRetentionAccessor {
    @Invoker("onStatusEffectApplied")
    void invokeOnStatusEffectApplied(StatusEffectInstance instance);
    @Accessor("activeStatusEffects")
    Map<StatusEffect, StatusEffectInstance> getActiveStatusEffects();
    @Invoker("canHaveStatusEffect")
    boolean invokeCanHaveStatusEffect(StatusEffectInstance instance);
}
