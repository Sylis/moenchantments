package com.biom4st3r.moenchantments.mixin.grapnel;

import java.util.function.Consumer;

import net.minecraft.entity.Entity;
import net.minecraft.network.Packet;
import net.minecraft.server.network.EntityTrackerEntry;
import net.minecraft.server.world.ServerWorld;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

/**
 * EntityTrackerEntryMxn
 */
@Mixin(EntityTrackerEntry.class)
public abstract class EntityTrackerEntryMxn {

    @Shadow @Final private Entity entity;
    @Shadow @Final private ServerWorld world;
    
    @Inject(
        at = @At(
            value = "INVOKE",
            target = "java/util/function/Consumer.accept(Ljava/lang/Object;)V",
            ordinal = 0,
            shift = Shift.AFTER),
        method = "sendPackets(Ljava/util/function/Consumer;)V",
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE
    )
    public void biom4st3r_sendAdditionalData(Consumer<Packet<?>> sender, CallbackInfo ci) {
        // if (GrapnelLogic.VALID_ARROW_ENTITY.contains(entity.getType())) {
        //     EnchantableProjectileEntity target = (EnchantableProjectileEntity) entity;
        //     Packet<?> packet = Packets.SERVER.createAdditionalArrowData(target);
        //     if (packet != null) {
        //         sender.accept(packet);
        //     }
        // }
    }
    
}