package com.biom4st3r.moenchantments.mixin.potion_retention;

import java.util.Iterator;
import java.util.List;

import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.logic.RetainedPotion;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.projectile.thrown.PotionEntity;
import net.minecraft.util.math.Box;


@Mixin(PotionEntity.class)
public abstract class ThrownPotionEntityMixin {
    @Inject(
        at=@At(
            value = "INVOKE_ASSIGN", 
            target="net/minecraft/entity/LivingEntity.addStatusEffect(Lnet/minecraft/entity/effect/StatusEffectInstance;)Z"
            ),
        method="applySplashPotion",
        locals = LocalCapture.CAPTURE_FAILHARD)
    private void addSplashPotionToWeapon(List<StatusEffectInstance> statusEffects, Entity targetEntity, 
                CallbackInfo ci, Box box, List<LivingEntity> entitiesInBox, Iterator<LivingEntity> entities, 
                LivingEntity nextEntity, double double1, double double2, Iterator<StatusEffectInstance> effects, 
                StatusEffectInstance currInstance, StatusEffect currEffect, int duration) {
        StatusEffectInstance statuseffect = statusEffects.get(0);

        if (targetEntity instanceof LivingEntity && !targetEntity.world.isClient()) {
            LivingEntity target = (LivingEntity)targetEntity;

            RetainedPotion.borrowFirstRetainedPotion(rt -> {
                if (rt.isEmpty() || (rt.getEffect() == statuseffect.getEffectType() && rt.getAmp() == statuseffect.getAmplifier()) ) {
                    int tokens = (int) Math.ceil( (duration / 20) / 10 );
                    rt.setPotionEffectAndCharges(statuseffect, tokens);
                }
            }, target.getMainHandStack(), target.getOffHandStack());
            ModInit.logger.debug(statuseffect.getEffectType().getTranslationKey());
        } else if (nextEntity != null) {
            RetainedPotion.borrowFirstRetainedPotion(rt -> {
                if (rt.isEmpty() || (rt.getEffect() == statuseffect.getEffectType() && rt.getAmp() == statuseffect.getAmplifier()) ) {
                    int tokens = (int)Math.ceil((duration / 20)/(10));
                    rt.setPotionEffectAndCharges(new StatusEffectInstance(currEffect, 0, statuseffect.getAmplifier()), tokens);
                }
                if (rt != null) rt.applyAndRelease();
            }, nextEntity.getMainHandStack(), nextEntity.getOffHandStack());
        }
    }
}