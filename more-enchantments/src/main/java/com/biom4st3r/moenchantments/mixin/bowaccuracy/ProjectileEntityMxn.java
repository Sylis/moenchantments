package com.biom4st3r.moenchantments.mixin.bowaccuracy;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import biom4st3r.libs.moenchant_lib.interfaces.EnchantableProjectileEntity;

@Mixin(ProjectileEntity.class)
public abstract class ProjectileEntityMxn extends Entity implements EnchantableProjectileEntity {

    public ProjectileEntityMxn(EntityType<?> type, World world) {
        super(type, world);
    }

    @Inject(at = @At(value = "INVOKE", target = "net/minecraft/entity/projectile/ProjectileEntity.setVelocity(Lnet/minecraft/util/math/Vec3d;)V", shift = Shift.AFTER), method = "setVelocity(DDDFF)V", locals = LocalCapture.CAPTURE_FAILHARD)
    public void applyAccuracyEnchantments(double x, double y, double z, float speed, float divergence, CallbackInfo ci,
            Vec3d vec3d) {
        if (((ProjectileEntity) (Object) this) instanceof ArrowEntity) {
            if (this.getEnchantmentLevel(EnchantmentRegistry.BOW_ACCURACY) > 0) {
                double magicNumber = 0.007499999832361937D * (this.getEnchantmentLevel(EnchantmentRegistry.BOW_ACCURACY) == 1 ? 0.5D : 0D);
                vec3d = new Vec3d(x, y, z).normalize()
                        .add(this.random.nextGaussian() * magicNumber * divergence,
                                this.random.nextGaussian() * magicNumber * divergence,
                                this.random.nextGaussian() * magicNumber * divergence)
                        .multiply(speed);

            } else if (this.getEnchantmentLevel(EnchantmentRegistry.BOW_ACCURACY_CURSE) > 0) {
                double magicNumber = 0.03D;
                vec3d = new Vec3d(x, y, z).normalize()
                        .add(this.random.nextGaussian() * magicNumber * divergence,
                                this.random.nextGaussian() * magicNumber * divergence,
                                this.random.nextGaussian() * magicNumber * divergence)
                        .multiply(speed);
            }
            this.setVelocity(vec3d);
        }
    }




}