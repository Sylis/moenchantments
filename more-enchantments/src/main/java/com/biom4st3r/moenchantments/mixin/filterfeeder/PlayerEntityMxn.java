package com.biom4st3r.moenchantments.mixin.filterfeeder;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.biom4st3r.moenchantments.logic.FilterFeeder;
import com.biom4st3r.moenchantments.logic.FilterFeeder.DUMMYFOODITEM;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({PlayerEntity.class})
public abstract class PlayerEntityMxn {
    @Shadow public abstract ItemStack getEquippedStack(EquipmentSlot slot);

    @Unique
    private DUMMYFOODITEM borrowedItem;
    @ModifyArg(
        method = "eatFood",
        at = @At(value = "INVOKE", target = "net/minecraft/entity/player/HungerManager.eat(Lnet/minecraft/item/Item;Lnet/minecraft/item/ItemStack;)V", ordinal = 0),
        index = 0
    )
    private Item moenchantment$getItem(Item i) {
        if (FilterFeeder.hasValidItemAndServer(this)) {
            borrowedItem = FilterFeeder.borrow(i.getFoodComponent());
            return borrowedItem;
        }
        return i;
    }
    @Inject(
        at = @At("TAIL"), 
        method = "eatFood", 
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE)
    private void moenchantment$returnItem(CallbackInfoReturnable<ItemStack> ci) {
        if (borrowedItem != null) {
            borrowedItem.release();
            borrowedItem = null;
        }
    }
}
