package com.biom4st3r.moenchantments.mixin.filterfeeder;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.item.SuspiciousStewItem;

import com.biom4st3r.moenchantments.logic.FilterFeeder;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin({SuspiciousStewItem.class})
public class SuspiciousStewItemMxn {
    @Redirect(
        method = "finishUsing",
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/entity/LivingEntity.addStatusEffect(Lnet/minecraft/entity/effect/StatusEffectInstance;)Z",
            ordinal = 0))
    private boolean replaceSusStewItemForFilterFeeder(LivingEntity entity, StatusEffectInstance instance) {
        if (FilterFeeder.hasValidItem(entity)) {
            return entity.addStatusEffect(FilterFeeder.getInverse(instance));
        } else {
            return entity.addStatusEffect(instance);
        }
    }
}
