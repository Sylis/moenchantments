package com.biom4st3r.moenchantments.mixin.curseoftheend;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.util.hit.EntityHitResult;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin({PersistentProjectileEntity.class})
public class PersistentProjectileEntityMxn {
    @Inject(
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/entity/Entity.damage(Lnet/minecraft/entity/damage/DamageSource;F)Z",
            shift = Shift.AFTER
            ),
        method = "onEntityHit",
        cancellable = true)
    private void moenchantment$isEndermanOrEnderProtection(EntityHitResult entityHitResult, CallbackInfo ci) {
        if (entityHitResult.getEntity() instanceof LivingEntity) {
            int lvl = EnchantmentHelper.getEquipmentLevel(EnchantmentRegistry.ENDERPROTECTION, (LivingEntity) entityHitResult.getEntity());
            if (lvl == 1 || lvl == 2) {
                ci.cancel();
            }
        }
    }
}
