package com.biom4st3r.moenchantments.mixin.notadatafixer;

import net.minecraft.util.Identifier;

import com.biom4st3r.moenchantments.ModInit;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin({Identifier.class})
public class Identifiermxn {
    private static String moenchantments$oldIdentifier = "biom4st3rmoenchantments";
    @Inject(at = @At(value = "INVOKE", target = "java/lang/Object.<init>()V", shift = Shift.AFTER), method = "<init>([Ljava/lang/String;)V")
    private void moenchantments_fix_id(String[] strings, CallbackInfo ci) {
        if (strings[0].equals(moenchantments$oldIdentifier)) {
            strings[0] = ModInit.MODID;
        }
    }
}
