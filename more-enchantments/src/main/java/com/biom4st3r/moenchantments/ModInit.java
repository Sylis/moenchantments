package com.biom4st3r.moenchantments;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.loader.api.FabricLoader;

import com.biom4st3r.moenchantments.logic.AlphafireLogic;
import com.biom4st3r.moenchantments.logic.EventCollection;
import com.biom4st3r.moenchantments.logic.VeinMinerLogic;
import com.biom4st3r.moenchantments.networking.Packets;

public class ModInit implements ModInitializer {
    public static final String MODID = "moenchantments".intern();
    public static final BioLogger logger = new BioLogger("Mo'Enchantments");
    public static boolean lockAutoSmeltSound = false;
    public static boolean extraBowsFound = false;

    @Override
    public void onInitialize() {
        extraBowsFound = FabricLoader.getInstance().isModLoaded("extrabows");
        EnchantmentRegistry.classLoad();
        EventCollection.init();
        Packets.classLoad();
        CommandRegistrationCallback.EVENT.register(new Commands());
        ServerLifecycleEvents.SERVER_STARTED.register(server -> {
            ModInit.logger.log("...and my turn again");
            ModInit.whitelistToBlock();
            ModInit.blacklistAutosmelt();
        });
    }

    public static void whitelistToBlock() {
        for (String s : MoEnchantsConfig.config.veinMinerBlockWhiteList) {
            Block block = Registry.BLOCK.get(new Identifier(s));
            if (block != Blocks.AIR) {
                AlphafireLogic.blocks_without_loot_function.add(block);
                VeinMinerLogic.int_block_whitelist.add(Registry.BLOCK.getRawId(block));
            } else {
                logger.log("%s was not found", s);
            }
        }
    }

    public static void blacklistAutosmelt() {
        for (String s : MoEnchantsConfig.config.AutoSmeltBlackList) {
            Item i = Registry.ITEM.get(new Identifier(s));
            if (i != Items.AIR) {
                AlphafireLogic.autoSmelt_blacklist.add(i);
            } else {
                logger.log("%s was not found", s);
            }
        }
    }

}