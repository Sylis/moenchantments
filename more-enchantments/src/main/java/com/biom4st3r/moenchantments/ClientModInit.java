package com.biom4st3r.moenchantments;

import net.fabricmc.api.ClientModInitializer;

public class ClientModInit implements ClientModInitializer {

    @Override
    public void onInitializeClient() {
        ClientEventHandlers.init();
    }
}
