package com.biom4st3r.moenchantments.networking;

import java.util.Optional;

import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.interfaces.EndStepper;
import com.biom4st3r.moenchantments.logic.EndStepProgressCounter;
import com.biom4st3r.moenchantments.logic.EndStepProgressCounter.Stage;

import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;

/**
 * Packets
 */
public final class Packets {

    private static Identifier ENDSTEP_UPDATE_STAGE = new Identifier(ModInit.MODID, "endsteppacket");
    private static Identifier ENDSTEP_INIT = new Identifier(ModInit.MODID, "endstepinit");
    public static PacketByteBuf newPbb() {
        return new PacketByteBuf(Unpooled.buffer());
    }

    public static final class SERVER
    {
        public static void classLoad(){
        }
        public static void updateEndStepState(ServerPlayerEntity pe, Stage stage) {
            PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
            buf.writeEnumConstant(stage);
            ServerPlayNetworking.send(pe, ENDSTEP_UPDATE_STAGE, buf);
        }
        public static void initEndStep(ServerPlayerEntity pe, PacketByteBuf buf) {
            ServerPlayNetworking.send(pe, ENDSTEP_INIT, buf);
        }
    }

    public static final class CLIENT
    {
        static
        {
            ClientPlayNetworking.registerGlobalReceiver(ENDSTEP_INIT, (client, handler, buf, sender) -> {
                ((EndStepper)client.player).moenchantment$setStepProgress(Optional.of(new EndStepProgressCounter(client.player, buf)));
            });
            ClientPlayNetworking.registerGlobalReceiver(ENDSTEP_UPDATE_STAGE, (client, handler, buf, sender) -> {
                ((EndStepper)client.player).moenchantment$getStepProgress().get().update(buf.readEnumConstant(Stage.class));
            });
        }
        public static void classLoad() { // load class
        }
    }
    
    public static void classLoad() { // load class
        SERVER.classLoad();
        if (FabricLoader.getInstance().getEnvironmentType() != EnvType.SERVER) CLIENT.classLoad();
    }
    
}