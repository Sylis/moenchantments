package biom4st3r.libs.moenchant_lib;

import java.util.List;
import java.util.Optional;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.screen.AnvilScreenHandler;


public interface ExtendedEnchantment {

    static Optional<ExtendedEnchantment> isExtended(Enchantment e) {
        return ExtendedEnchantment.cast(e).isExtended() ? Optional.of((ExtendedEnchantment) e) : Optional.empty();
    }
    
    boolean isExtended();

    /**
     * When enchantment {@code ExtendedEnchantment#isExtended()} this is <p>
     * this is used to replace {@code EnchantmentTarget#isAcceptableItem(net.minecraft.item.Item)}
     * in {@code EnchantmentHelper#getPossibleEntries(int, ItemStack, boolean)}
     * @param is
     * @return
     */
    boolean isAcceptable(ItemStack is);

    /**
     * When enchantment {@code ExtendedEnchantment#isExtended()} this is <p>
     * used to replace {@code Enchantment#isAcceptableItem(ItemStack)}
     * in {@code AnvilScreenHandler#updateResult()}
     * @param is
     * @param anvil
     * @return
     */
    boolean isAcceptibleInAnvil(ItemStack is, AnvilScreenHandler anvil);

    /**
     * When enchantment {@code ExtendedEnchantment#isExtended()} this is <p>
     * checked in {@code EnchantmentHelper#getPossibleEntries(int, ItemStack, boolean)}<p>
     * in addition to of stack.getItem() == Items.BOOK.
     * @return
     */
    boolean isAcceptibleOnBook();

    /**
     * Wheather or not this enchantment is valid for EnchantRandomlyLootFunction
     * @param stack
     * @param context
     * @return
     */
    boolean isAcceptableForRandomLootFunc(ItemStack stack, LootContext context);

    /**
     * Helper Method
     * @return
     */
    default Enchantment asEnchantment() {
        return ((Enchantment)this);
    }

    /**
     * Helper Method
     * @param e
     * @return
     */
    static ExtendedEnchantment cast(Enchantment e) {
        return (ExtendedEnchantment)e;
    }

    /**
     * Whether or not this enchantment has it's own custom conditions for being applied
     * @return
     */
    default boolean providesApplicationLogic() {
        return false;
    }
    
    /**
     * if {@code ExtendedEnchantment#providesApplicationLogic()} <p>
     * this will fully replace logic in {@code getPossibleEnchantments}<p>
     * for your enchantment.
     * @param power
     * @param stack
     * @param treasureAllowed
     * @param entries
     */
    void applicationLogic(int power, ItemStack stack, boolean treasureAllowed, List<EnchantmentLevelEntry> entries);

    /**
     * Called after an enchantment has been added to the tag of the stack AKA at the end of {@link ItemStack#addEnchantment(Enchantment, int)}
     * @param stack
     */
    default void enchantmentAddedToStack(ItemStack stack) {}
}
