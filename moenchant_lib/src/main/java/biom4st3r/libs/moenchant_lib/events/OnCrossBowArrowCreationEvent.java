package biom4st3r.libs.moenchant_lib.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.ItemStack;

public interface OnCrossBowArrowCreationEvent {
    
    Event<OnCrossBowArrowCreationEvent> EVENT = EventFactory.createArrayBacked(OnCrossBowArrowCreationEvent.class,
        listeners  ->  (arrow, crossbow, arrowItem, le)  ->  {
            for (OnCrossBowArrowCreationEvent event : listeners) {
                event.onCreation(arrow, crossbow, arrowItem, le);
            }
    });
    void onCreation(PersistentProjectileEntity arrow, ItemStack crossBow, ItemStack arrowItem, LivingEntity shooter);
}