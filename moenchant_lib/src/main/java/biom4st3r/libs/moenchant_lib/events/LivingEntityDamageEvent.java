package biom4st3r.libs.moenchant_lib.events;

import net.minecraft.entity.Entity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.util.ActionResult;
import net.minecraft.util.TypedActionResult;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.impl.base.event.EventFactoryImpl;

/**
 * LivingEntityDamageCallback
 */

@FunctionalInterface
public interface LivingEntityDamageEvent {
    static final TypedActionResult<Float> PASS = TypedActionResult.pass(0F);
    static final TypedActionResult<Float> SUCCESS = TypedActionResult.success(0F);
    static final TypedActionResult<Float> FAIL = TypedActionResult.fail(0F);

    Event<LivingEntityDamageEvent> EVENT = EventFactoryImpl.createArrayBacked(LivingEntityDamageEvent.class, 
        listeners ->  (damageSource, damage, e) -> {
            TypedActionResult<Float> consumed = null;
            for (LivingEntityDamageEvent callback : listeners) {
                TypedActionResult<Float> result = callback.onDamage(damageSource, damage, e);
                if (result.getResult() == ActionResult.FAIL) {
                    return result;
                } else if (result.getResult() == ActionResult.CONSUME) {
                    consumed = result;
                }
            }
            return consumed == null ? TypedActionResult.pass(damage) : consumed;
        });

    TypedActionResult<Float> onDamage(DamageSource damageSource, float damage, Entity target);

}