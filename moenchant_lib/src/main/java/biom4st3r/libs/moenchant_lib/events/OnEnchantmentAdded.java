package biom4st3r.libs.moenchant_lib.events;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;

/**
 * Event for when NOT EXTENDED enchantments are added to an itemstack. If you want an extended enchantment just override ExtendedEnchantment#enchantmentAddedToStack 
 */
public interface OnEnchantmentAdded {
    
    
    Event<OnEnchantmentAdded> EVENT = EventFactory.createArrayBacked(OnEnchantmentAdded.class, 
        listeners ->(enchant, lvl, stack) -> {
            for (OnEnchantmentAdded event : listeners) {
                event.onAdded(enchant, lvl, stack);
            }
    });
    
    void onAdded(Enchantment enchantment, int level, ItemStack stack);
}
