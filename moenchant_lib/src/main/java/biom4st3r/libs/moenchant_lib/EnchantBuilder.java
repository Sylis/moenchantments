package biom4st3r.libs.moenchant_lib;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantment.Rarity;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.util.registry.Registry;

import biom4st3r.libs.moenchant_lib.config_test.EnchantmentOverride;
import biom4st3r.libs.moenchant_lib.events.OnBowArrowCreationEvent;
import biom4st3r.libs.moenchant_lib.events.OnCrossBowArrowCreationEvent;
import biom4st3r.libs.moenchant_lib.interfaces.EnchantableProjectileEntity;
import biom4st3r.net.objecthunter.exp4j.Expression;
import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * MoEnchantBuilder
 * Enchantment Builder
 */
public class EnchantBuilder {
    EnchantmentSkeleton DELEGATE;

    
    public static interface GenericArrowCreativeEvent {
        void onCreation(PersistentProjectileEntity arrow, ItemStack crossBow, ItemStack arrowItem, LivingEntity shooter);
    }

    public static interface PseudoGetPossibleEnchantments {
        void apply(int power, ItemStack stack, boolean treasureAllowed, List<EnchantmentLevelEntry> list);
    }


    public EnchantBuilder(Rarity rarity, EquipmentSlot... slots) {
        Preconditions.checkNotNull(rarity, "MoenchantmentBuilder ctor(rarity) does not accept null" );
        Preconditions.checkNotNull(slots, "MoenchantmentBuilder ctor(slots) does not accept null");
        this.DELEGATE = new EnchantmentSkeleton(rarity, slots);
    }

    private EnchantBuilder(EnchantmentSkeleton skeleton) {
        this.DELEGATE = skeleton;
    }

    /**
     * When this enchantment is added to an itemstack.
     * @param consumer
     * @return
     */
    public EnchantBuilder whenAddedToStack(Consumer<ItemStack> consumer) {
        Preconditions.checkNotNull(consumer, "MoenchantmentBuilder.whenAddedToStack() does not accept null");
        this.DELEGATE.whenAddedToStack = consumer;
        return this;
    }

    /**
     * If this enchantment can be applied from the enchantment table or enchantment loot functions
     * @param bool
     * @return
     */
    public EnchantBuilder fromEnchantmentTableAndLoot(boolean bool) {
        this.DELEGATE.isAvailableForRandomSelection = bool;
        return this;
    }

    /**
     * If villagers and offer this enchantment
     * @param bool
     * @return
     */
    public EnchantBuilder fromLibrarian(boolean bool) {
        this.DELEGATE.isAvailableForEnchantedBookOffer = bool;
        return this;
    }

    /**
     * Wheather or not this enchantments should be registered
     * @param bool
     * @return
     */
    public EnchantBuilder enabled(boolean bool) {
        this.DELEGATE.enabled = bool;
        return this;
    }

    /**
     * TODO
     * @param bool
     * @return
     */
    public EnchantBuilder treasure(boolean bool) {
        this.DELEGATE.isTreasure = bool;
        return this;
    }

    /**
     * TODO
     * @param bool
     * @return
     */
    public EnchantBuilder curse(boolean bool) {
        this.DELEGATE.isCurse = bool;
        return this;
    }

    /**
     * Sets {@code ExtendedEnchantment#providesApplicationLogic()} to true and <p>
     * provides the alternative logic to be used and {@link net.minecraft.enchantment.EnchantmentHelper#getPossibleEntries(int, ItemStack, boolean)}
     * @param func
     * @return
     */
    public EnchantBuilder setApplicationLogic(PseudoGetPossibleEnchantments func) {
        Preconditions.checkNotNull(func, "MoenchantmentBuilder.setApplicationLogic() does not accept null");
        this.DELEGATE.hasApplyLogic = true;
        this.DELEGATE.applyLogic = func;
        return this;
    }

    /**
     * 
     * @param i
     * @return
     */
    public EnchantBuilder minlevel(int i) {
        Preconditions.checkArgument(i > 0, "MoenchantmentBuilder.minlevel() does not accept Less than 0");
        this.DELEGATE.minlevel = i;
        return this;
    }

    /**
     * 
     * @param i
     * @return
     */
    public EnchantBuilder maxlevel(int i) {
        Preconditions.checkArgument(i > 0, "MoenchantmentBuilder.maxlevel() does not accept Less than 0");
        this.DELEGATE.maxlevel = i;
        return this;
    }

    /**
     * 
     * @param provider
     * @return
     */
    public EnchantBuilder minpower(Function<Integer, Integer> provider) {
        Preconditions.checkNotNull(provider, "MoenchantmentBuilder.minpower() does not accept null");
        this.DELEGATE.minpower = provider;
        return this;
    }

    /**
     * 
     * @param provider
     * @return
     */
    public EnchantBuilder maxpower(Function<Integer, Integer> provider) {
        Preconditions.checkNotNull(provider, "MoenchantmentBuilder.maxpower() does not accept null");
        this.DELEGATE.maxpower = provider;
        return this;
    }

    /**
     * Specifies enchantments that should be on the same item as this.
     * @param e
     * @return
     */
    public EnchantBuilder addExclusive(Enchantment e) {
        Preconditions.checkNotNull(e, "MoenchantmentBuilder.addExclusive() does not accept null");
        this.DELEGATE.exclusiveEnchantments.add(e);
        return this;
    }

    /**
     * Specifies enchantments that should be on the same item as this.
     * @param e
     * @return
     */
    public EnchantBuilder addExclusive(Enchantment... e) {
        Preconditions.checkNotNull(e, "MoenchantmentBuilder.addExclusive() does not accept null");
        Collections.addAll(this.DELEGATE.exclusiveEnchantments, e);
        return this;
    }

    /**
     * Provides extra logic for when a bow with this enchantment creates an arrow
     * @param event
     * @return
     */
    public EnchantBuilder onArrowCreatedFromBow(OnBowArrowCreationEvent event) {
        OnBowArrowCreationEvent.EVENT.register(event);
        return this;
    }

    /**
     * Provides extra logic for when a bow or crossbow with this enchantment creates an arrow
     * @param event
     * @return
     */
    public EnchantBuilder onArrowCreated(GenericArrowCreativeEvent event) {
        Preconditions.checkNotNull(event, "MoenchantmentBuilder.onArrowCreated() does not accept null");

        OnCrossBowArrowCreationEvent.EVENT.register((arrow, crossBow, arrowItem, shooter)-> {
            event.onCreation(arrow, crossBow, arrowItem, shooter);
        });
        OnBowArrowCreationEvent.EVENT.register((bow, arrowStack, arrowEntity, player, elapsed, pullProg)-> {
            event.onCreation(arrowEntity, bow, arrowStack, player);
        });
        return this;
    }

    /**
     * Provides extra logic for when a crossbow with this enchantment creates an arrow
     * @param event
     * @return
     */
    public EnchantBuilder onArrowCreatedFromCrossbow(OnCrossBowArrowCreationEvent event) {
        Preconditions.checkNotNull(event, "MoenchantmentBuilder.onArrowCreatedFromCrossbow() does not accept null");
        OnCrossBowArrowCreationEvent.EVENT.register(event);
        return this;
    }

    /**
     * Addeds the enchantment to arrows created from bows with this enchantment. Similar to what Vanilla does with punch<p>
     * to check an Projectile entity with an enchantments use EnchantableProjectileEntity
     * @return
     */
    public EnchantBuilder applyEnchantmentToArrowsFromBows() {
        this.init_data_tracker_for_arrows();
        OnBowArrowCreationEvent.EVENT.register((bow, arrowStack, arrowEntity, player, elapsed, pullProg)-> {
            EnchantableProjectileEntity.enchantProjectile(arrowEntity, this.DELEGATE, this.DELEGATE.getLevel(bow));
        });
        return this;
    }

    /**
     * Addeds the enchantment to arrows created from bows or crossbows with this enchantment. Similar to what Vanilla does with punch<p>
     * to check an Projectile entity with an enchantments use EnchantableProjectileEntity
     * @return
     */
    public EnchantBuilder applyEnchantmentToArrows() {
        this.init_data_tracker_for_arrows();
        this.applyEnchantmentToArrowsFromBows();
        this.applyEnchantmentToArrowsFromCrossbows();
        return this;
    }

    private void init_data_tracker_for_arrows() {
        if(this.DELEGATE.PROJECTILE_TRACKED_DATA != null) return;
        this.DELEGATE.PROJECTILE_TRACKED_DATA = DataTracker.registerData(ProjectileEntity.class, TrackedDataHandlerRegistry.BYTE);
        EnchantmentSkeleton.TRACKED_ARROW_DATA_KEYS.put(this.DELEGATE, this.DELEGATE.PROJECTILE_TRACKED_DATA);
    }

    /**
     * Addeds the enchantment to arrows created from crossbows with this enchantment. Similar to what Vanilla does with punch<p>
     * to check an Projectile entity with an enchantments use EnchantableProjectileEntity
     * @return
     */
    public EnchantBuilder applyEnchantmentToArrowsFromCrossbows() {
        this.init_data_tracker_for_arrows();
        OnCrossBowArrowCreationEvent.EVENT.register((arrow, crossbow, ArrowItem, shooter)-> {
            EnchantableProjectileEntity.enchantProjectile(arrow, this.DELEGATE, this.DELEGATE.getLevel(crossbow));
        });
        return this;
    }

    /**
     * Used for everything unless otherwise specified within this Builder.
     * @param isAcceptible
     * @return
     */
    public EnchantBuilder isAcceptible(Predicate<ItemStack> isAcceptible) {
        Preconditions.checkNotNull(isAcceptible, "MoenchantmentBuilder.isAcceptible() does not accept null");
        this.DELEGATE.isAcceptible = isAcceptible;
        return this;
    }


    public EnchantBuilder isAcceptibleInAnvil(BiFunction<ItemStack, AnvilScreenHandler, Boolean> isAcceptible) {
        Preconditions.checkNotNull(isAcceptible, "MoenchantmentBuilder.isAcceptibleInAnvil() does not accept null");
        this.DELEGATE.isAcceptibleInAnvil = isAcceptible;
        return this;
    }

    static Gson gson = new GsonBuilder().registerTypeAdapter(EnchantmentOverride.class, new EnchantmentOverride()).create();

    public EnchantmentSkeleton build(String regname) {
        Preconditions.checkNotNull(regname, "MoenchantmentBuilder.build() does not accept null");
        Preconditions.checkArgument(!regname.isEmpty(), "MoenchantmentBuilder.build() does not accept empty regnames");
        
        LibInit.enchantmentOverrides.reset();
        if (!LibInit.enchantmentOverrides.isInvalid() && LibInit.enchantmentOverrides.get(regname).isPresent()) {
            EnchantmentOverride override = gson.fromJson(LibInit.enchantmentOverrides.getCurrentElement(), EnchantmentOverride.class);
            if (override.curse != null) {
                this.curse(override.curse);
            }
            if (override.enabled != null) {
                this.enabled(override.enabled);
            }
            if (override.treasure != null) {
                this.treasure(override.treasure);
            }
            if (override.max_level != null) {
                this.maxlevel(override.max_level);
            }
            if (override.min_level != null) {
                this.minlevel(override.min_level);
            }
            if (override.min_power != null) {
                Expression ex = override.min_power;
                this.minpower(i  ->  (int)ex.setVariable("level", i).evaluate());
            }
            if (override.max_power != null) {
                Expression ex = override.max_power;
                this.maxpower(i  ->  (int)ex.setVariable("level", i).evaluate());
            }
        }
        this.DELEGATE.regname = regname.toLowerCase();
        if (this.DELEGATE.enabled()) {
            ENCHANTMENTS.add(this.DELEGATE);
        }
        return this.DELEGATE;
    }

    private static final Set<EnchantmentSkeleton> ENCHANTMENTS = Sets.newHashSet();
    private static EnchantmentSkeleton[] INTERNAL_ENCHANTMENTS = null;
    
    public static final Stream<EnchantmentSkeleton> getEnchantments() {
        if (INTERNAL_ENCHANTMENTS == null) INTERNAL_ENCHANTMENTS = ENCHANTMENTS.toArray(new EnchantmentSkeleton[0]);
        return Stream.of(INTERNAL_ENCHANTMENTS);
    }

    /**
     * You probably don't need this.
     * @param enchant
     * @param consumer
     */
    @Deprecated()
    public static void modEnchantment(EnchantmentSkeleton enchant, Consumer<EnchantBuilder> consumer) {
        if (Registry.ENCHANTMENT.getRawId(enchant) != -1) throw new IllegalStateException("Enchantments can't be modified after being registered.");
        consumer.accept(new EnchantBuilder(enchant));
    }

}