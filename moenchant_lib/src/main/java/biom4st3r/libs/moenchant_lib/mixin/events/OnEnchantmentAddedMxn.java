package biom4st3r.libs.moenchant_lib.mixin.events;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import biom4st3r.libs.moenchant_lib.events.OnEnchantmentAdded;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;

@Mixin({ItemStack.class})
public class OnEnchantmentAddedMxn {
    @Inject(
        at = @At("TAIL"), 
        method = "addEnchantment", 
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE)
    private void moenchantlib$onAdded(Enchantment enchantment, int level, CallbackInfo ci) {
        if (ExtendedEnchantment.cast(enchantment).isExtended()) ExtendedEnchantment.cast(enchantment).enchantmentAddedToStack((ItemStack)(Object)this);
        else OnEnchantmentAdded.EVENT.invoker().onAdded(enchantment, level, (ItemStack)(Object)this);
    }
}
