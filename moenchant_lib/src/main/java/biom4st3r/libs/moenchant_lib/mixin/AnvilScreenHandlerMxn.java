package biom4st3r.libs.moenchant_lib.mixin;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.AnvilScreenHandler;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(AnvilScreenHandler.class)
public abstract class AnvilScreenHandlerMxn {
    
    @Redirect(
        method = "updateResult",
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/enchantment/Enchantment.isAcceptableItem(Lnet/minecraft/item/ItemStack;)Z",
            ordinal = 0))
    private boolean replaceIsAcceptibleItem(Enchantment enchant, ItemStack is) {
        if (ExtendedEnchantment.cast(enchant).isExtended()) {

            ExtendedEnchantment ex = ExtendedEnchantment.cast(enchant);
            return ex.isExtended() ? ex.isAcceptibleInAnvil(is, (AnvilScreenHandler)(Object)this) : enchant.isAcceptableItem(is);
        } else {
            return enchant.isAcceptableItem(is);
        }
    }
}
