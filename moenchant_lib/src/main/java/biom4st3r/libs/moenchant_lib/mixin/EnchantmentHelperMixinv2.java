
package biom4st3r.libs.moenchant_lib.mixin;

import java.util.Iterator;
import java.util.List;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

import biom4st3r.libs.moenchant_lib.EnchantmentSkeleton;
import biom4st3r.libs.moenchant_lib.EnchantBuilder;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(EnchantmentHelper.class)
public abstract class EnchantmentHelperMixinv2 {

     @Inject(method = "getPossibleEntries", at = @At(value = "RETURN"), locals = LocalCapture.NO_CAPTURE, cancellable = true)
     private static void re_add_moenchntments(int power, ItemStack stack, boolean treasureAllowed,
               CallbackInfoReturnable<List<EnchantmentLevelEntry>> ci) {
          Iterator<EnchantmentSkeleton> iter = EnchantBuilder.getEnchantments().iterator();
          List<EnchantmentLevelEntry> list = ci.getReturnValue();
          while (iter.hasNext()) {
               EnchantmentSkeleton enchant = iter.next();
               if (enchant.providesApplicationLogic()) {
                    enchant.applicationLogic(power, stack, treasureAllowed, list);
               } else {
                    boolean success = true;
                    boolean isBook = stack.getItem() == Items.BOOK;

                    success &= enchant.isAcceptableItem(stack) || (isBook && enchant.isAcceptibleOnBook());

                    success &= !(enchant.isTreasure() && !treasureAllowed);

                    success &= enchant.isAvailableForRandomSelection();

                    if (!success)
                         continue;
                         
                    for (int lvl = enchant.getMaxLevel(); lvl > enchant.getMinLevel() - 1; --lvl) {
                         if (power >= enchant.getMinPower(lvl) && power <= enchant.getMaxPower(lvl)) {
                              list.add(new EnchantmentLevelEntry(enchant, lvl));
                         }
                    }
               }
          }
     }
}
