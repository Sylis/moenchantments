package biom4st3r.libs.moenchant_lib.mixin.events;

import java.util.concurrent.locks.ReentrantLock;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.util.ActionResult;
import net.minecraft.util.TypedActionResult;

import biom4st3r.libs.moenchant_lib.events.LivingEntityDamageEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(LivingEntity.class)
public abstract class LivingEntityDamageMxn {
    @Inject(
        at = @At("HEAD"),
        method = "damage",
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    public void LivingEntityEvent(DamageSource source, float damage, CallbackInfoReturnable<Boolean> ci) {
        if (!((LivingEntity)(Object)this).world.isClient) {
            TypedActionResult<Float> result = LivingEntityDamageEvent.EVENT.invoker().onDamage(source, damage, (Entity)(Object)this);
            if (result.getResult() == ActionResult.FAIL) {
                ci.cancel();
            } else if (result.getResult() == ActionResult.CONSUME) {
                moenchantments$lock.lock();
                moenchantments$newDamage = result.getValue();
            }
        }
    }
    private Float moenchantments$newDamage = null;
    private ReentrantLock moenchantments$lock = new ReentrantLock();

    @ModifyVariable(
        at = @At(value = "HEAD", shift = Shift.BY, by = 3),
        method = "damage",
        argsOnly = true, 
        index = 2,
        print = false
    )
    public float changeDamage(float damage) {
        if (moenchantments$newDamage != null) {
            damage = moenchantments$newDamage.floatValue();
            moenchantments$newDamage = null;
            moenchantments$lock.unlock();
        }
        return damage;
    }
}