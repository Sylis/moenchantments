package biom4st3r.libs.moenchant_lib.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.Optional;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.World;

import biom4st3r.libs.moenchant_lib.EnchantmentSkeleton;
import biom4st3r.libs.moenchant_lib.interfaces.EnchantableProjectileEntity;

@Mixin(ProjectileEntity.class)
public abstract class ProjectileEntityEnchantmentImpl extends Entity implements EnchantableProjectileEntity {
    
    public ProjectileEntityEnchantmentImpl(EntityType<?> type, World world) {
        super(type, world);
    }

    @Inject(at = @At(value = "RETURN"), method = "<init>*", cancellable = false, locals = LocalCapture.NO_CAPTURE)
    public void ctorInject(CallbackInfo ci) {
        for(TrackedData<Byte> i : EnchantmentSkeleton.TRACKED_ARROW_DATA_KEYS.values()) {
            this.getDataTracker().startTracking(i, (byte)0);
        }
    }
    
    @Override
    public int getEnchantmentLevel(Enchantment e) {
        Optional<EnchantmentSkeleton> o = EnchantmentSkeleton.isSkeleton(e);
        if(o.isPresent()) {
            return o.get().getLevelFromProjectile((ProjectileEntity)(Object)this);
        }
        return 0;
    }

    @Override
    public void setEnchantmentLevel(Enchantment e, int level) {
        Optional<EnchantmentSkeleton> o = EnchantmentSkeleton.isSkeleton(e);
        if(o.isPresent()) {
            o.get().setLevelFromProjectile((ProjectileEntity)(Object)this, level);
        }
    }

    @Inject(
        at = @At(value = "TAIL"),
        method = "readCustomDataFromTag",
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE
    )
    public void biom4st3r_readCustomData(CompoundTag tag, CallbackInfo ci) {
        CompoundTag ct = tag.getCompound("biom4st3r_enchantments");
        EnchantmentSkeleton.TRACKED_ARROW_DATA_KEYS.keySet().forEach((k)-> {
            this.getDataTracker().set(k.getTrackedData(), ct.getByte(k.regName()));
        });
    }

    @Inject(
        at = @At(value = "TAIL"),
        method = "writeCustomDataToTag",
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE
    )
    public void biom4st3r_writeCustomData(CompoundTag tag, CallbackInfo ci) {
        CompoundTag ct = new CompoundTag();
        EnchantmentSkeleton.TRACKED_ARROW_DATA_KEYS.forEach((enchant,trackeddata)-> {
            ct.putByte(enchant.regName(), this.getDataTracker().get(trackeddata));
        });
        tag.put("biom4st3r_enchantments", ct);
    }
    
}