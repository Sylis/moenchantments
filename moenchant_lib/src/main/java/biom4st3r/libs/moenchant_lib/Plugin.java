package biom4st3r.libs.moenchant_lib;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import net.fabricmc.loader.api.FabricLoader;

import it.unimi.dsi.fastutil.objects.Object2BooleanMap;
import it.unimi.dsi.fastutil.objects.Object2BooleanOpenHashMap;
import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

public class Plugin implements IMixinConfigPlugin {
    public static <T> T make(T obj, Consumer<T> consumer) {
        consumer.accept(obj);
        return obj;
    }
    public static Object2BooleanMap<String> disabler = make(new Object2BooleanOpenHashMap<>(), map  ->  {
        String prefix = "biom4st3r.libs.moenchant_lib.mixin.";
        map.put(prefix + "EnchantmentHelperInDev", FabricLoader.getInstance().isDevelopmentEnvironment());
        map.put(prefix + "EnchantmentHelperInProd", !FabricLoader.getInstance().isDevelopmentEnvironment());
    });


    @Override
    public void onLoad(String mixinPackage) {
    }

    @Override
    public String getRefMapperConfig() {
        return null;
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        boolean b = disabler.getOrDefault(mixinClassName, true);
        LibInit.logger.debug("Applying %s: %s", mixinClassName, b);
        return b;
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {
        
    }

    @Override
    public List<String> getMixins() {
        return null;
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {
        
    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {
        
    }
    
}
