package biom4st3r.libs.moenchant_lib;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import biom4st3r.libs.moenchant_lib.config_test.EnchantmentOverride.JsonHandler;
import net.fabricmc.loader.api.FabricLoader;

public class LibInit {
    public static final BioLogger logger = new BioLogger("Mo'Enchant-Lib");

    public static JsonHandler enchantmentOverrides; 

    static {
        try {
            File file = new File(FabricLoader.getInstance().getConfigDir().toFile(), "moenchantment_overrides.json");
            if (!file.exists()) file.createNewFile();
            byte[] b = Files.readAllBytes(file.toPath());
            Gson builder = new GsonBuilder().create();
            enchantmentOverrides = JsonHandler.of(builder.fromJson(new String(b), JsonObject.class));
        } catch (IOException e) {
            logger.log("moenchantment_override not found");
        }
    }
}
