package biom4st3r.libs.moenchant_lib.interfaces;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.projectile.ProjectileEntity;

public interface EnchantableProjectileEntity 
{
    public void setEnchantmentLevel(Enchantment e, int level);
    public int getEnchantmentLevel(Enchantment e);
    static void enchantProjectile(ProjectileEntity entity, Enchantment enchant, int level) {
        ((EnchantableProjectileEntity)entity).setEnchantmentLevel(enchant, level);
    }
    static void enchantProjectile(ProjectileEntity entity, EnchantmentLevelEntry entry) {
        enchantProjectile(entity, entry.enchantment, entry.level);
    }
}