package biom4st3r.libs.moenchant_lib;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.Util;
import net.minecraft.util.registry.Registry;

import biom4st3r.libs.moenchant_lib.EnchantBuilder.PseudoGetPossibleEnchantments;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.jetbrains.annotations.ApiStatus.Internal;

/**
 * Buildable enchantment implementation
 */
public class EnchantmentSkeleton extends Enchantment implements ExtendedEnchantment {

    public static Optional<EnchantmentSkeleton> isSkeleton(Enchantment e) {
        return (e instanceof EnchantmentSkeleton) ? Optional.of((EnchantmentSkeleton)e) : Optional.empty();
    }

    public boolean isEnabled() {
        return enabled;
    }

    boolean enabled = false;
    boolean isTreasure = super.isTreasure();
    boolean isCurse = super.isCursed();
    String regname = "";
    int minlevel = super.getMinLevel();
    int maxlevel = super.getMaxLevel();
    boolean isAvailableForEnchantedBookOffer = super.isAvailableForEnchantedBookOffer();
    boolean isAvailableForRandomSelection = super.isAvailableForRandomSelection();
    Predicate<ItemStack> isAcceptible = stack -> {
        LibInit.logger.debug("Please replace isAcceptible for %s", regname);
        return false;
    };
    BiFunction<ItemStack, AnvilScreenHandler, Boolean> isAcceptibleInAnvil = (is, as) -> this.isAcceptible.test(is);
    PseudoGetPossibleEnchantments applyLogic = (a, b, c, d) -> {};
    Function<Integer, Integer> minpower = level -> super.getMinPower(level);
    Function<Integer, Integer> maxpower = level -> minpower.apply(level)+20;
    List<Enchantment> exclusiveEnchantments = Lists.newArrayList();
    boolean hasApplyLogic = false;
    boolean isAcceptibleOnBook = true;
    BiFunction<ItemStack, LootContext, Boolean> isAcceptableForRandomLootFunc = (is, ctx) -> this.isAcceptible.test(is);
    Consumer<ItemStack> whenAddedToStack = stack ->{};

    TrackedData<Byte> PROJECTILE_TRACKED_DATA = null;
    public static final Map<EnchantmentSkeleton, TrackedData<Byte>> TRACKED_ARROW_DATA_KEYS = Maps.newHashMap();

    public TrackedData<Byte> getTrackedData() {
        return PROJECTILE_TRACKED_DATA;
    }

    public int getLevelFromProjectile(ProjectileEntity entity) {
        if(this.PROJECTILE_TRACKED_DATA == null) return 0;
        if(entity == null) return 0;
        return entity.getDataTracker().get(PROJECTILE_TRACKED_DATA);
    }    
    public void setLevelFromProjectile(ProjectileEntity entity, int level) {
        if(this.PROJECTILE_TRACKED_DATA == null) return;
        if(entity == null) return;
        entity.getDataTracker().set(PROJECTILE_TRACKED_DATA, (byte)level);
    }

    private final static TreeMap<Integer, String> romanNumeralMap = Util.make(new TreeMap<Integer, String>(), map ->
    {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");
    });

    @Override
    public void enchantmentAddedToStack(ItemStack stack) {
        this.whenAddedToStack.accept(stack);
    }

    @Override
    public boolean providesApplicationLogic() {
        return hasApplyLogic;
    }

    @Override
    public boolean isAvailableForEnchantedBookOffer() {
        return isAvailableForEnchantedBookOffer;
    }

    @Override
    public boolean isAvailableForRandomSelection() {
        return isAvailableForRandomSelection;
    }

    @Override
    public Text getName(int level) {
        MutableText text = new TranslatableText(this.getTranslationKey(), new Object[0]);
        if (this.isCursed()) {
            text.formatted(Formatting.RED);
        } else {
            text.formatted(Formatting.GRAY);
        }
        if (level != 1 || this.getMaxLevel() != 1) {
            text.append(" ").append(new TranslatableText(toRoman(level)));
        }
        return text;
    }

    public static String toRoman(int num) {
        //Stackoverflow
        int l = romanNumeralMap.floorKey(num);
        if (num == l) {
            return romanNumeralMap.get(num);
        }
        return romanNumeralMap.get(l) + toRoman(num - l);
    }

    public interface LevelProvider
    {
        public int supply(int level);
    }

    public EnchantmentSkeleton(Rarity r, EquipmentSlot[] es) {
        super(r, null, es);
        this.exclusiveEnchantments.add(this);
    }

    @Override
    public boolean isCursed() {
        return isCurse;
    }
    @Override
    public boolean isTreasure() {
        return isTreasure;
    }

    @Internal
    public String regName() {
        if (this.regname.isEmpty()) throw new IllegalStateException("EnchantmentSkeleton#regName called before enchantment was built");
        return regname;
    }

    public boolean enabled() {
        return enabled;
    }

    @Override
    public int getMaxLevel() {
        return maxlevel;
    }

    @Override
    public int getMinLevel() {
        return minlevel;
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) {
        return isAcceptible.test(iS);
    }

    @Override
    protected boolean canAccept(Enchantment other) {
        return !exclusiveEnchantments.contains(other);
    }

    @Override
    public int getMinPower(int level) {
        return minpower.apply(level);
    }

    @Override
    public int getMaxPower(int level) {
        
        return maxpower.apply(level);
    }

    public boolean hasEnchantment(ItemStack i) {
        return EnchantmentHelper.getLevel(this, i) > 0;
    }

    public int getLevel(ItemStack i) {
        return EnchantmentHelper.getLevel(this, i);
    }

    public static boolean hasEnchant(Enchantment e, ItemStack i) {
        return EnchantmentHelper.getLevel(e, i) > 0;
    }

    @Override
    public final boolean isExtended() {
        return true;
    }

    public final EnchantmentSkeleton register() {
        if (this.enabled) {
            final Identifier id = this.regName().contains(":") ? new Identifier(this.regname) : new Identifier("moenchantments", this.regname);
            Registry.register(Registry.ENCHANTMENT, id, this);
        }
        return this;
    }

    @Override
    public boolean isAcceptable(ItemStack is) {
        return this.isAcceptible.test(is);
    }

    @Override
    public boolean isAcceptibleInAnvil(ItemStack is, AnvilScreenHandler anvil) {
        return this.isAcceptibleInAnvil.apply(is, anvil);
    }

    @Override
    public boolean isAcceptableForRandomLootFunc(ItemStack stack, LootContext context) {
        return this.isAcceptableForRandomLootFunc.apply(stack, context);
    }

    @Override
    public boolean isAcceptibleOnBook() {
        return isAcceptibleOnBook;
    }

    @Override
    public void applicationLogic(int power, ItemStack stack, boolean treasureAllowed,
            List<EnchantmentLevelEntry> entries) {
        this.applyLogic.apply(power, stack, treasureAllowed, entries);
    }
}