# Incorrect Spacing on commas (arg0,arg1,arg2) -> (arg0, arg1, arg2)
* (\w),(\w)
* $1, $2
# replace C style brackets with java
* \)[ ]{0,}\n[ ]{0,}\{
* ) {
# removed brackets from single arg lambda
* \(([a-zA-Z0-9]{1,})\)(\W{0,})->
* $1$2->
# Replace (notspace)-> with (notspace) ->
* ([^\s])->
* $1 ->
# Replace 1line lambda bodies (asdf)->{return true;}
* ->([ ]{0,})\{return (.*);\}
* ->$1$2
# for|if followed by non-space
* (\W)(for|if)(\()
* $1$2 $3