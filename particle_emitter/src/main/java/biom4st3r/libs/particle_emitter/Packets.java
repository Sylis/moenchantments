package biom4st3r.libs.particle_emitter;

import biom4st3r.libs.particle_emitter.interfaces.ParticleEmittingWorld;
import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;

public class Packets {
    public static Identifier SEND_PARTICLE_EMITTER = new Identifier(LibInit.MODID,"sndpartem");
    public static Identifier KILL_PARTICLE_EMITTER = new Identifier(LibInit.MODID,"kllpartem");
    public static PacketByteBuf newPbb() {
        return new PacketByteBuf(Unpooled.buffer());
    }
    public static final void classLoad() {
        SERVER.classLoad();
        if (FabricLoader.getInstance().getEnvironmentType() != EnvType.SERVER) CLIENT.classLoad();
    }
    public static final class SERVER {
        
        public static final void classLoad() {
            
        }
        public static void sendParticleEmiiter(ServerPlayerEntity pe, PacketByteBuf emitter) {
            ServerPlayNetworking.send(pe, SEND_PARTICLE_EMITTER, emitter);
        }
        public static void killParticleEmiiter(ServerPlayerEntity pe, long id) {
            PacketByteBuf pbb = newPbb();
            pbb.writeLong(id);
            ServerPlayNetworking.send(pe, KILL_PARTICLE_EMITTER, pbb);
        }
    }
    
    public static final class CLIENT {
        
        public static final void classLoad() {
            
        }
        static {
            ClientPlayNetworking.registerGlobalReceiver(SEND_PARTICLE_EMITTER, (client, handler, buf, sender) -> {
                ((ParticleEmittingWorld)client.world).addEmitter(ParticleEmitter.fromPacket(buf, client.world));
            });
            ClientPlayNetworking.registerGlobalReceiver(KILL_PARTICLE_EMITTER, (client, handler, buf, sender) -> {
                ((ParticleEmittingWorld)client.world).markAsDead(buf.readLong());
            });
        }
    }
}
