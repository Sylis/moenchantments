package biom4st3r.libs.particle_emitter;

import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Predicate;

import net.minecraft.client.MinecraftClient;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.s2c.play.ParticleS2CPacket;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleType;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Tickable;
import net.minecraft.util.Util;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;

import biom4st3r.libs.particle_emitter.interfaces.ParticleEmittingWorld;
import biom4st3r.net.objecthunter.exp4j.Expression;
import biom4st3r.net.objecthunter.exp4j.ExpressionBuilder;
import com.google.common.collect.Maps;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import org.jetbrains.annotations.ApiStatus.AvailableSince;
import org.jetbrains.annotations.ApiStatus.Internal;

public class ParticleEmitter implements Tickable {
    final ParticleEffect type;
    final Vec3d pos;
    final String stringVelocityX;
    final String stringVelocityY;
    final String stringVelocityZ;
    final Expression velocityX;
    final Expression velocityY;
    final Expression velocityZ;
    boolean dead;
    final World world;
    final Random random;
    public final long ID;
    int lifetime;

    public static interface ParticleContext {
        double test(int tick, Random rand, Vec3d pos);
    }

    /**
     * 
     * @param type
     * @param pos
     * @param vx are mathmatical expresssions supporting the varibles randomdouble, randomint, randomgaussian, tick, pos.x, pos.y, pos.z
     * @param vy
     * @param vz
     * @param world
     * @param lifeTime
     * @return
     */
    @AvailableSince("0.1.0")
    public static Optional<ParticleEmitter> of(ParticleEffect type, Vec3d pos, String vx, String vy,
    String vz, World world, int lifeTime) {
        if (world.isClient) return Optional.empty(); 
        if (lifeTime == 1) {
            final ParticleS2CPacket packet = new ParticleS2CPacket(type, true, pos.x, pos.y, pos.z, 0, 0, 0, 1, 0);
            forPlayerInRange(world, pos, player -> {
                player.networkHandler.connection.send(packet);
            });
            return Optional.empty();
        }
        return Optional.of(new ParticleEmitter(type, pos, vx, vy, vz, world, lifeTime));
    }

    @AvailableSince("0.1.0")
    public void begin() {
        if (this.world.isClient) {
            throw new IllegalAccessError("Partical Emitters should never be started from the client. Honestly i'm not even sure how to got one of these on your client; I explicitly block that from happening...unless you were messing around in ClientWorld in which case, why? that seems so unnessisary?");
        }
        ((ParticleEmittingWorld)this.world).addEmitter(this);
    }

    /**
     * @param type
     * @param pos
     * @param velocityX
     * @param velocityY
     * @param velocityZ
     * @param dead
     * @param world
     */
    @Internal
    private ParticleEmitter(ParticleEffect type, Vec3d pos, String vx, String vy,
            String vz, World world, int lifeTime) {
        this.type = type;
        this.pos = pos;
        this.stringVelocityX = vx;
        this.stringVelocityY = vy;
        this.stringVelocityZ = vz;
        this.velocityX = getBuilder(vx).build();
        this.velocityY = getBuilder(vy).build();
        this.velocityZ = getBuilder(vz).build();
        this.dead = false;
        this.world = world;
        this.random = new Random(world.random.nextLong());
        this.lifetime = lifeTime;
        this.ID = random.nextLong();
    }

    @Internal
    private ParticleEmitter(ParticleEffect type, Vec3d pos, String vx, String vy,
        String vz, World world, int lifeTime, long id) {
        this.type = type;
        this.pos = pos;
        this.stringVelocityX = vx;
        this.stringVelocityY = vy;
        this.stringVelocityZ = vz;
        this.velocityX = getBuilder(vx).build();
        this.velocityY = getBuilder(vy).build();
        this.velocityZ = getBuilder(vz).build();
        this.dead = false;
        this.world = world;
        this.random = new Random(world.random.nextLong());
        this.lifetime = lifeTime;
        this.ID = id;
    }

    private static ExpressionBuilder getBuilder(String expression) {
        ExpressionBuilder builder = new ExpressionBuilder(expression);
        builder.variables(variables.keySet());
        return builder;
    }

    public static final String 
        RANDOM_DOUBLE = "randomdouble",
        RANDOM_GAUSS = "randomguassian",
        RANDOM_INT = "randomint",
        TICK = "tick",
        POSX = "pos.x",
        POSY = "pos.y",
        POSZ = "pos.z"
        ;

    
    private static final Map<String, ParticleContext> variables = Util.make(Maps.newHashMap(), map -> {
        map.put(RANDOM_DOUBLE, (ticks, rand, pos)->rand.nextDouble());
        map.put(RANDOM_GAUSS, (ticks, rand, pos)->rand.nextGaussian());
        map.put(RANDOM_INT, (ticks, rand, pos)->rand.nextInt());
        map.put(TICK, (ticks,rand,pos)-> ticks);
        map.put(POSX, (ticks,rand,pos)-> pos.x);
        map.put(POSY, (ticks,rand,pos)-> pos.y);
        map.put(POSZ, (ticks,rand,pos)-> pos.z);
    });

    @AvailableSince("0.1.0")
    public static void registerVariable(String name, ParticleContext func) {
        variables.put(name, func);
    }

    private Expression fillExpression(Expression express) {
        for (String s : express.getVariableNames()) {
            ParticleContext i = variables.get(s);
            if (i != null) {
                express.setVariable(s, i.test(this.lifetime, this.random, this.pos));
            } else {
                throw new IllegalArgumentException(s + " is not a valid varibles for ParticleEmitter");
            }
        }
        return express;
    }

    public boolean isDead() {
        return this.dead;
    }

    private IntSet leachers = new IntOpenHashSet();
    private IntSet peers = new IntOpenHashSet();

    @AvailableSince("0.1.0")
    public void kill() {
        this.dead = true;
        forPlayerInRange(player -> peers.contains(player.getEntityId()), player -> {
            Packets.SERVER.killParticleEmiiter(player, this.ID);
        });
    }

    private static void forPlayerInRange(World world, Vec3d pos, Consumer<ServerPlayerEntity> consumer) {
        if (!world.isClient) {
            for (ServerPlayerEntity player : ((ServerWorld)world).getPlayers()) {
                if (pos.subtract(player.getPos()).length() < RANGE) {
                    consumer.accept(player);
                }
            }
        }
    }    
    private void forPlayerInRange(Consumer<ServerPlayerEntity> consumer) {
        if (!world.isClient) {
            for (ServerPlayerEntity player : ((ServerWorld)world).getPlayers()) {
                if (this.pos.subtract(player.getPos()).length() < RANGE) {
                    consumer.accept(player);
                }
            }
        }
    }
    private void forPlayerInRange(Predicate<ServerPlayerEntity> filter, Consumer<ServerPlayerEntity> consumer) {
        if (!world.isClient) {
            for (ServerPlayerEntity player : ((ServerWorld)world).getPlayers()) {
                if (!filter.test(player)) continue;
                if (this.pos.subtract(player.getPos()).length() < RANGE) {
                    consumer.accept(player);
                }
            }
        }
    }

    static final int RANGE = 120;

    @Internal
    @Environment(EnvType.CLIENT)
    @SuppressWarnings({"resource"})
    private void emitClient() {
        if (MinecraftClient.getInstance().player.getPos().subtract(pos).length() > RANGE) {
            this.kill();
            return;
        }
        switch(MinecraftClient.getInstance().options.particles) {
            case ALL:
                world.addImportantParticle((ParticleEffect)type, true, pos.x, pos.y, pos.z, fillExpression(velocityX).evaluate(), fillExpression(velocityY).evaluate(), fillExpression(velocityZ).evaluate());
                break;
            case DECREASED:
                if ((this.lifetime & 3) == 3) {
                    world.addImportantParticle((ParticleEffect)type, true, pos.x, pos.y, pos.z, fillExpression(velocityX).evaluate(), fillExpression(velocityY).evaluate(), fillExpression(velocityZ).evaluate());
                }
                break;
            case MINIMAL:
                if ((this.lifetime & 15) == 15) {
                    world.addImportantParticle((ParticleEffect)type, true, pos.x, pos.y, pos.z, fillExpression(velocityX).evaluate(), fillExpression(velocityY).evaluate(), fillExpression(velocityZ).evaluate());
                }
                break;
        }
    }

    @Internal
    public void emit() {
        if (world.isClient) {
            emitClient();
        } else {
            final Vec3d vec = new Vec3d(fillExpression(velocityX).evaluate(), fillExpression(velocityY).evaluate(), fillExpression(velocityZ).evaluate());
            // TODO fix these offset and speed variables. I have no idea what they look like
            final ParticleS2CPacket packet = new ParticleS2CPacket((ParticleEffect)type, true, pos.x, pos.y, pos.z, (float)vec.x, (float)vec.y, (float)vec.z, (float)vec.length(), 0);
            forPlayerInRange(player -> {
                if (leachers.contains(player.getEntityId())) {
                    player.networkHandler.connection.send(packet);
                    // System.out.println("Rendering Remotely");
                } else if (!peers.contains(player.getEntityId())) {
                    if (ServerPlayNetworking.canSend(player, Packets.SEND_PARTICLE_EMITTER)) {
                        peers.add(player.getEntityId());
                        Packets.SERVER.sendParticleEmiiter(player, this.toPacket());
                    } else {
                        this.leachers.add(player.getEntityId());
                    }
                }
            });
        }
    }

    @Internal
    public PacketByteBuf toPacket() {
        PacketByteBuf pbb = Packets.newPbb();
        pbb.writeIdentifier(Registry.PARTICLE_TYPE.getId(type.getType()));
        type.write(pbb);

        pbb.writeDouble(this.pos.x);
        pbb.writeDouble(this.pos.y);
        pbb.writeDouble(this.pos.z);
        pbb.writeString(this.stringVelocityX);
        pbb.writeString(this.stringVelocityY);
        pbb.writeString(this.stringVelocityZ);
        pbb.writeInt(this.lifetime);
        pbb.writeLong(this.ID);
        return pbb;
    }

    @Internal
    @SuppressWarnings({"unchecked"})
    public static ParticleEmitter fromPacket(PacketByteBuf buf, World world) {
        ParticleType<ParticleEffect> type = (ParticleType<ParticleEffect>) Registry.PARTICLE_TYPE.get(buf.readIdentifier());
        
        return new ParticleEmitter(type.getParametersFactory().read(type, buf), new Vec3d(buf.readDouble(),buf.readDouble(),buf.readDouble()), buf.readString(), buf.readString(), buf.readString(), world, buf.readInt(), buf.readLong());
    }

    @Internal
    @Override
    public void tick() {
        this.emit();
        if (--lifetime == 0) {
            this.kill();
        }
    }
}
